#include <bob/catalogs/csvcatalog.hpp>
#include <bob/catalogs/vaexcatalog.hpp>



int main(int argc, char *argv[]){

    std::string fname = "data/lbol-gdr2_0_1000000.csv";

    // Define mapping ================================
    xt::xarray<std::string> mapping = {
            {"name"              , "source_id" , "-"}              ,
            {"parallax"          , "parallax"  , "parallax_error"} ,
            {"Gaia_MAW_BP_faint" , "bpmag"     , "bpmag_error"}    ,
            {"Gaia_MAW_G"        , "gmag"      , "gmag_error"}     ,
            {"Gaia_MAW_RP"       , "rpmag"     , "rpmag_error"}    ,
            {"2MASS_J"           , "j_m"       , "j_msigcom"}      ,
            {"2MASS_H"           , "h_m"       , "h_msigcom"}      ,
            {"2MASS_Ks"          , "ks_m"      , "ks_msigcom"}     ,
            {"WISE_RSR_W1"       , "w1mpro"    , "w1mpro_error"}   ,
            {"WISE_RSR_W2"       , "w2mpro"    , "w2mpro_error"}
    };

    // convert to base class
    auto cat0 = Catalog::CsvCatalog(fname, mapping);
    auto cat1 = Catalog::VaexCatalog("data/testsample.vaex.hdf5", mapping);
    Catalog::Base* cat = &cat0;

    std::cout << *cat << "\n";

    std::cout << cat->get_star(0);
    std::cout << cat->get_star(1);
    // reset file stream
    std::cout << cat->get_star(1);
    // continues with current file (skip entries)
    std::cout << cat->get_star(4);

    cat = &cat1;

    std::cout << *cat << "\n";
    std::cout << cat->get_star(0);
    std::cout << cat->get_star(1);
    std::cout << cat->get_star(1);
    std::cout << cat->get_star(4);


    /*
     * TODO:
     *
     * [ ] update main_mars and main to use Catalog::Base
     */

    return 0;
}