#!/usr/bin/env python3
import warnings
import numpy as np
import pandas
import pickle
import datetime
from sympy import printing
from pyearth import Earth, export
from ezdata.datashader import DSPlotter
from ezdata.matplotlib import add_image_legend
from scipy.ndimage import gaussian_filter
from tqdm import tqdm

warnings.simplefilter(action='ignore', category=FutureWarning)


def clean_name(pred_name):
    """ Clear label names """
    if 'faint' in pred_name.lower():
        return 'BP'
    else:
        return pred_name.split('_')[-1]


class cxx_exporter:
    """ Generate some cxx code snippets automatically """

    @staticmethod
    def get_generated_mark():
        return "generated on {0:s}".format(
            datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

    @staticmethod
    def remove_indent(text):
        """ Remove all indentation from (multiline) text """
        return '\n'.join([k.strip() for k in text.split('\n')])

    @staticmethod
    def add_indent(text, indent=0):
        """ Add indentation from (multiline) text """
        if indent <= 0:
            return text
        return '\n'.join([' ' * indent + k for k in text.split('\n')])

    @staticmethod
    def export_vector(vec, indent=0):
        """ Generate a vector declaration """
        ndim = np.ndim(vec)
        if ndim > 1:
            lines = []
            for row in vec:
                line = ' ' * int(indent) + '{' + ','.join([str(k) for k in row]) + '}'
                lines.append(line)
            return '{' + ',\n'.join(lines).strip() + '}'
        else:
            return '{' + ','.join([str(k) for k in vec]) + '}'

    @staticmethod
    def generate_header(text, indent=0):
        header = "\n/**\n"
        header += '\n'.join([' * ' + k for k in text.split('\n')])
        header += '\n * ' + '\n * Code ' + cxx_exporter.get_generated_mark()
        header += "\n */"
        return cxx_exporter.add_indent(header, indent)


class InitialGuess:
    """ Find an initial guess from a subset of true model points

    precomputes SED predictions for N points and use these as reference
    SEDs to get a good guess of where to start
    """
    def __init__(self, Xref, sednames, models):
        """ Constructor

        Parameters
        ----------
        Xref: ndarray(Nmodels, Npars)
            AP models of reference
        sednames: sequence(str)
            ordered names of the SED model vector
        models: dict
            mars models
        """
        self.sednames = sednames
        self.X = Xref
        self.y = np.array([models[k].predict(self.X)
                           for k in self.sednames]).T

    def get_guess(self, y, dmod, yerr):
        """ Find a good guess from a given SED and distance modulus

        Parameters
        ----------
        y: ndarray(Nsed,)
            single sed
        dmod:float
            distance modulus
        yerr: ndarray(Nsed,)
            single sed uncertainties
        """
        ind = np.isfinite(y) & np.isfinite(yerr)
        r = np.sum((y[ind] - (self.y[:, ind] + dmod)) ** 2, axis=1)
        return self.X[np.argmin(r)]

    def export_cxx(self, namespace=None):

        header = cxx_exporter.generate_header(
            "Initial guess configuration\n\n    This space contains a random subset of APs"
        )

        code = "std::vector<std::vector<double>> reference_X = "
        code += "{0:s};".format(
            cxx_exporter.export_vector(self.X.round(5),
            indent=len(code) + 1)
        )

        if namespace is not None:
            code = cxx_exporter.add_indent(code, 5)
            code = "namespace {0:s} {{\n\n{1:s}\n\n}};// end namespace {0:s}\n".format(namespace, code)

        return header + '\n' + code


class boundary_2D:
    """Find a 2D boundary curve around the data that authorizes the point to exist.

    This version does not assume that all valid point are part of a continuous
    distribution.
    """
    def __init__(self):
        # mask: ndarray<K, L>
        self.mask = None
        # x_coords: ndarray<K>
        self.x_coords = None
        # coords: ndarray<L>
        self.y_coords = None
        # names of x, y
        self.names = None

    def fit(self, X,
            dyn = 0.01,
            smooth = 0.2,
            weights=None, names=None):
        self.names = names
        bins = [np.arange(xk.min() - 10 * dyn * xk.ptp(),
                          xk.max() + 10 * dyn * xk.ptp(),
                          dyn * xk.ptp())
                for xk in X.T]
        n, b0, b1 = np.histogram2d(X[:, 0], X[:, 1],
                                   bins=bins, weights=weights)
        self.hist = (n, b0, b1)
        mask = gaussian_filter(n, smooth) > 0
        self.mask = mask
        x_, y_ = 0.5 * (b0[1:] + b0[:-1]), 0.5 * (b1[1:] + b1[:-1])
        self.x_coords = x_
        self.y_coords = y_
        return self

    def predict(self, X):

        nx = len(self.x_coords)
        ny = len(self.y_coords)
        dx = self.x_coords[1] - self.x_coords[0]
        xmin = self.x_coords[0]
        dy = self.y_coords[1] - self.y_coords[0]
        ymin = self.y_coords[0]

        indx = ((X[:, 0] - xmin) / dx).astype(int)
        indy = ((X[:, 1] - ymin) / dy).astype(int)

        valid = (indx >= 0) & (indx < nx) & (indy >= 0) & (indy < ny)

        indx = np.clip(indx, 0, nx - 1)
        indy = np.clip(indy, 0, ny - 1)

        return valid.astype(float) * self.mask[indx, indy]

    @staticmethod
    def _export_vector_cxx(vec):
        ndim = np.ndim(vec)
        if ndim > 1:
            lines = []
            for row in vec:
                line = '{' + ','.join([str(k) for k in row]) + '}'
                lines.append(line)
            return '{' + ',\n'.join(lines) + '}'
        else:
            return '{' + ','.join([str(k) for k in vec]) + '}'

    def export_cxx(self, namespace=None):

        header = cxx_exporter.generate_header(
                "Boundary 2D of the {self.name} dimensions"
            )

        decl = "std::vector<std::vector<double>> mask = "
        code = decl + "{0:s};\n".format(
            cxx_exporter.export_vector(self.mask.astype(int), indent=len(decl))
        )
        decl = "std::vector<double> x_coords = "
        code += decl + "{0:s};\n".format(
            cxx_exporter.export_vector(
                self.x_coords.astype(float).round(5), indent=len(decl))
        )
        decl = "std::vector<double> y_coords = "
        code += decl + "{0:s};\n".format(
            cxx_exporter.export_vector(
                self.y_coords.astype(float).round(5), indent=len(decl))
        )

        if namespace is not None:
            code = cxx_exporter.add_indent(code, 5)
            code = "namespace {0:s} {{\n\n{1:s}\n\n}};// end namespace {0:s}\n".format(namespace, code)

        return header + '\n' + code


class CascadeModel:
    """ Model definition that cascades multiple MARS models.
    using the input features and the residuals from the previous level
    (i.e, each level is a correction model of the previous layer)
    aka Bagging method.
    """
    def __init__(self, degrees, **kwargs):
        # the polynomial degree explored in the fit
        self.degrees = degrees
        # the names of the input features
        self.xlabels = kwargs.pop('xlabels', None)
        # anything going to Earth model
        self.kwargs = kwargs
        # saving models per level
        self.models = []

    def fit(self, X, y):
        """ Fit the input features to the output 1D label """
        models = []
        y_hats = []
        y_pred = np.zeros_like(y.ravel())
        residuals = [y.ravel()]
        labels = self.xlabels
        X_model = X
        for level, degree in enumerate(tqdm(self.degrees)):
            model = Earth(max_degree=degree, **self.kwargs)
            y_model = residuals[level]
            if level > 0:
                if self.xlabels is None:
                    self.xlabels = list(models[-1].xlabels_)
                labels = self.xlabels + ['ypred_{0:d}'.format(level)]
                X_model = np.hstack([X, y_pred[:, None]])
            model.fit(X_model, y_model, xlabels=labels)
            models.append(model)
            y_hat = model.predict(X_model)
            y_pred += y_hat
            y_hats.append(y_hat)
            residuals.append(y.ravel() - y_pred)
        self.models = models
        return self

    def predict(self, X0):
        """ Predict the output label for features X0 """
        y0_pred = np.zeros(len(X0))
        X0_model = X0
        for level, model in enumerate(self.models):
            if level > 0:
                X0_model = np.hstack([X0, y0_pred[:, None]])
            y0_pred += model.predict(X0_model)
        return y0_pred


def generate_model_name(models=None, shape=None, pars_columns=None, prefix=None):
    if models is not None:
        ref_model = list(models.values())[0]
        shape = ref_model.degrees
        pars_columns = ref_model.xlabels

    fname = 'mars_shape_' + '-'.join(map(str, shape)) + \
        'from_' + '-'.join(pars_columns)
    if prefix is not None:
        return prefix + '_' + fname
    return fname


def plot_diagnostics_residuals(models, ndf, pred_columns):
    import pylab as plt
    p = DSPlotter(ndf)
    columns = [k for k in pred_columns if k in ndf.columns]

    ref_model = list(models.values())[0]
    pars_columns = ref_model.xlabels

    plt.figure(figsize=(4 * len(columns), 10))

    for num, pred_name in enumerate(columns, 1):
        plt.subplot(3, len(columns), num)
        cleaned_name = clean_name(pred_name)
        if pred_name[:2] == 'A_':
            cleaned_name = 'A(' + cleaned_name + ')'
        if pred_name != 'logT':
            p.plot('logT', pred_name, norm='log10', alpha_below=1, color='b',
                   label='True')
        p.plot('logT', pred_name + '_pred', norm='log10', alpha_below=1,
               color='r', label='Pred')
        plt.ylabel(cleaned_name)
        if num == 1:
            add_image_legend(merge=True, frameon=False, loc='best')
        plt.title(cleaned_name)

    for num, pred_name in enumerate(columns, len(columns) + 1):
        plt.subplot(3, len(columns), num)
        cleaned_name = clean_name(pred_name)
        a = p.plot(pred_name, pred_name + '_res', alpha_below=1)
        plt.colorbar(a)
        rms = np.sqrt(np.mean(ndf[pred_name + '_res'] ** 2))
        mean = np.mean(ndf[pred_name + '_res'])
        sigma = np.std(ndf[pred_name + '_res'])
        txt = f'rms={rms:0.3f}, $\mu$={mean:0.3f}, $\sigma$={sigma:0.3f}'
        plt.title(txt, fontsize='x-small')
        ylim = np.max(np.abs(plt.ylim()))
        plt.ylim(-ylim, ylim)
        plt.xlabel(cleaned_name)
        plt.ylabel(cleaned_name + ' residuals')

    feats = np.array([[mk.feature_importances_[:len(pars_columns)]
                       for mk in m.models]
                      for m in models.values()])

    for num, (pred_name, image) in enumerate(zip(columns, feats), 2 * len(columns) + 1):
        plt.subplot(3, len(columns), num)
        cleaned_name = clean_name(pred_name)
        plt.imshow(image.T, vmin=0, vmax=1, aspect='auto')
        plt.colorbar().set_label('importance')
        plt.yticks(np.arange(len(pars_columns)), pars_columns)
        plt.xticks(np.arange(image.shape[1]))
        plt.xlim(-0.5, image.shape[0] - 0.5)
        plt.xlabel('level')
        plt.ylabel('feature')

    # frac_models = X.shape[0] * 1. / X0.shape[0]
    # plt.suptitle('MARS models trained on {0:0.1f}%'.format(frac_models * 100))

    fname = generate_model_name(models) + '_ap.png'

    plt.tight_layout()
    plt.savefig(fname, bbox_inches='tight')


def export_cpp_model(models, output='mars_model.hpp'):

    rootfilename = '.'.join(output.split('.')[:-1])
    with open(rootfilename + '.pickle', 'wb') as out:
        pickle.dump(models, out)

    with open(rootfilename + '.pickle', 'rb') as infile:
        models = pickle.load(infile)

    code = ""
    mappings = []
    for name_, mod_ in models.items():
        for level_ in range(len(mod_.models)):
            model_ = mod_.models[level_]
            level_m1 = level_ - 1
            function_name = "model_{0:s}_level_{1:d}".format(name_, level_)
            cxxcode = printing.cxxcode(export.export_sympy(model_))
            declaration = "double {function_name:s} (const std::vector<double>& x) {{\n"
            declaration = declaration.format(function_name=function_name)
            declaration += "\n".join([f"    double {name:s} = x[{dim:d}];"
                                      for dim, name in enumerate(model_.xlabels_) if 'ypred' not in name])
            declaration += "\n" + "\n".join([f"    double {name:s} = model_{name_:s}_level_{level_m1:d}(x);"
                                      for name in model_.xlabels_ if 'ypred' in name])
            declaration += "\n double this_ypred = {0:s}; \n".format(cxxcode)
            if level_ > 0:
                declaration += "return this_ypred + ypred_{0:d}; \n}}".format(level_)
            else:
                declaration += "return this_ypred; \n}"
            code += '\n\n' + declaration
            final_call = "[](const std::vector<double> &x) {{ return mars_model::{0:s}(x);}}"
        mappings.append(final_call.format(function_name))

    model = list(models.values())[0]

    header = """
    /** Model definition using Multivariate Adaptive Regression with Splines (MARS).
     *  This model was trained offline with a python script.
     *
     *  Model generated on {0:s}
     */
    #pragma once
    #include <iostream>
    #include <math.h>
    #include <cmath>
    #include <cstdio>
    #include <vector>

    """.format(datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    header = cxx_exporter.remove_indent(header)

    final_code = """
    using model_fn = std::function<double(std::vector<double> &)>;

    std::vector<std::string> feature_names = {{ {2:s} }};
    std::vector<std::string> label_names = {{ {3:s} }};

    {0:s}

    std::vector<model_fn> model ({{
    {1:s}
    }});
    """.format(code, ',\n'.join(mappings),
               ', '.join(['"{0:s}"'.format(k) for k in model.xlabels]),
               ', '.join(['"{0:s}"'.format(k) for k in models.keys()]))

    code = cxx_exporter.add_indent(code, 5)
    final_code = "namespace mars_model {{\n\n{0:s}\n\n}};// end namespace {0:s}\n".format(final_code)

    with open(output, 'w') as outfile:
        outfile.write(header + final_code)


def cxx_X_limits(X0, namespace=None):
    header = cxx_exporter.generate_header("Limits of the input model data.")
    X0.min(axis=0), X0.max(axis=0)
    
    decl = "std::vector<double> features_min = "
    code = decl + "{0:s};\n".format(
        cxx_exporter.export_vector(
            np.min(X0, axis=0).round(5), indent=len(decl))
    )
    decl = "std::vector<double> features_max = "
    code += decl + "{0:s};\n".format(
        cxx_exporter.export_vector(
            np.max(X0, axis=0).round(5), indent=len(decl))
    )

    if namespace is not None:
        code = cxx_exporter.add_indent(code, 5)
        code = "namespace {0:s} {{\n\n{1:s}\n\n}};// end namespace {0:s}\n".format(namespace, code)

    return header + '\n' + code


def train_a0_logT_logL_R0_model():
    input_model_data = 'subdatamodels_20190403.hdf5'
    # input features
    pars_columns = ['A0', 'logT', 'logL', 'R0', 'logA']
    # selected labels
    phot_columns = ['Gaia_MAW_G', 'Gaia_MAW_BP_faint', 'Gaia_MAW_RP',
                    '2MASS_H', '2MASS_J', '2MASS_Ks',
                    'WISE_RSR_W1', 'WISE_RSR_W2',
                    ]
    ap_columns = ['logM', 'logg', 'log_R']
    # extra columns
    ext_columns = ['GROUND_JOHNSON_B', 'GROUND_JOHNSON_V', 'GROUND_COUSINS_I']
    # add extinction A_x for x in phot_columns
    ext_columns += [f'A_{x:s}' for x in phot_columns]
    ext_columns += ['A_B', 'A_V', 'A_I', 'A_H', 'A_J', 'A_Ks', 'A_W1', 'A_W2']

    # model shape: spline max degree per level
    shape = 3, 3, 2

    # Read all columns from input file
    all_columns = pars_columns + phot_columns + ap_columns + ext_columns
    pred_columns = phot_columns + ap_columns + ext_columns

    df = pandas.read_hdf(input_model_data, columns=all_columns)
    ndf = df
    X0 = df[list(pars_columns)].to_numpy()
    cut = slice(0, len(X0), 157)  # prime number
    X = X0[cut]

    pred_columns = [k for k in df.columns if k not in pars_columns]

    model_file_name = generate_model_name(shape=shape, pars_columns=pars_columns)

    try:
        with open(model_file_name + '.pickle', 'rb') as out:
            models = pickle.load(out)

        print(f'Using previous trained model {model_file_name:s}')
        print('recomputing residuals')
        for pred_name, model in tqdm(models.items()):
            y0 = df[pred_name].to_numpy()
            y0_pred = model.predict(X0)
            ndf[pred_name + '_pred'] = y0_pred
            ndf[pred_name + '_res'] = y0.ravel() - y0_pred
    except Exception as e:
        print(f'Previous trained model {model_file_name:s} not found')
        models = {}
        for pred_name in tqdm(pred_columns):
            print("Model: " + pred_name)
            y0 = df[pred_name].to_numpy()
            y = y0[cut]

            model = CascadeModel(shape, xlabels=pars_columns,
                                 use_fast=True, smooth=True, verbose=0,
                                 max_terms=100, feature_importance_type='gcv'
                                 )
            model.fit(X, y.ravel())
            y0_pred = model.predict(X0)

            ndf[pred_name + '_pred'] = y0_pred
            ndf[pred_name + '_res'] = y0.ravel() - y0_pred
            models[pred_name] = model

        with open(model_file_name + '.pickle', 'wb') as out:
            pickle.dump(models, out)

    print(f'exporting model to {model_file_name}.hpp')
    export_cpp_model(models, output=model_file_name + '.hpp')

    print(f'exporting prior to {model_file_name}.hpp')
    bound = boundary_2D().fit(X0[:, 1:3], names=['logT', 'logL'])
    with open(model_file_name + '.hpp', 'a+') as outf:
        outf.write(bound.export_cxx(namespace='mars_model_hrd_prior'))

    print(f'exporting initial guess to {model_file_name}.hpp')
    N = 1000
    ind = np.sort(np.random.choice(np.arange(len(X0)), N, replace=False))
    guess = InitialGuess(X0[ind], phot_columns, models)
    with open(model_file_name + '.hpp', 'a+') as outf:
        outf.write(guess.export_cxx(namespace="mars_model_guess"))

    print(f'exporting limits')
    with open(model_file_name + '.hpp', 'a+') as outf:
        outf.write(cxx_X_limits(X0, namespace='mars_model_limits'))

    print(f'plotting diagnostics to {model_file_name}.png')
    plot_diagnostics_residuals(models, ndf, pred_columns)

    return models, ndf, pred_columns


def train_parsec_model():
    input_model_data = 'parsecmodels.pandas.hdf5'
    model_prefix = 'parsec'
    # input features
    pars_columns = ['A0', 'logT', 'logL', 'mh', 'logA']
    # selected labels
    phot_columns = ['Gaia_MAW_G', 'Gaia_MAW_BP_faint', 'Gaia_MAW_RP',
                    '2MASS_H', '2MASS_J', '2MASS_Ks',
                    'WISE_RSR_W1', 'WISE_RSR_W2',
                    ]
    ap_columns = ['logM', 'logg', 'log_R']
    # extra columns
    ext_columns = ['GROUND_JOHNSON_B', 'GROUND_JOHNSON_V', 'GROUND_COUSINS_I']
    # add extinction A_x for x in phot_columns
    ext_columns += [f'A_{x:s}' for x in phot_columns]
    ext_columns += ['A_B', 'A_V', 'A_I', 'A_H', 'A_J', 'A_Ks', 'A_W1', 'A_W2']

    # model shape: spline max degree per level
    shape = 1, 2, 3, 4

    # Read all columns from input file
    all_columns = pars_columns + phot_columns + ap_columns + ext_columns
    pred_columns = phot_columns + ap_columns + ext_columns

    df = pandas.read_hdf(input_model_data, columns=all_columns)
    ndf = df
    X0 = df[list(pars_columns)].to_numpy()
    cut = slice(0, len(X0), 157)  # prime number
    X = X0[cut]

    pred_columns = [k for k in df.columns if k not in pars_columns]

    model_file_name = generate_model_name(shape=shape,
                                          pars_columns=pars_columns,
                                          prefix=model_prefix)

    try:
        with open(model_file_name + '.pickle', 'rb') as out:
            models = pickle.load(out)

        print(f'Using previous trained model {model_file_name:s}')
        print('recomputing residuals')
        for pred_name, model in tqdm(models.items()):
            y0 = df[pred_name].to_numpy()
            y0_pred = model.predict(X0)
            ndf[pred_name + '_pred'] = y0_pred
            ndf[pred_name + '_res'] = y0.ravel() - y0_pred
    except Exception as e:
        print(f'Previous trained model {model_file_name:s} not found')
        models = {}
        for pred_name in tqdm(pred_columns):
            print("Model: " + pred_name)
            y0 = df[pred_name].to_numpy()
            y = y0[cut]

            model = CascadeModel(shape, xlabels=pars_columns,
                                 use_fast=True, smooth=True, verbose=0,
                                 max_terms=100, feature_importance_type='gcv'
                                 )
            model.fit(X, y.ravel())
            y0_pred = model.predict(X0)

            ndf[pred_name + '_pred'] = y0_pred
            ndf[pred_name + '_res'] = y0.ravel() - y0_pred
            models[pred_name] = model

        with open(model_file_name + '.pickle', 'wb') as out:
            pickle.dump(models, out)

    print(f'exporting model to {model_file_name}.hpp')
    export_cpp_model(models, output=model_file_name + '.hpp')

    print(f'exporting prior to {model_file_name}.hpp')
    bound = boundary_2D().fit(X0[:, 1:3], names=['logT', 'logL'])
    with open(model_file_name + '.hpp', 'a+') as outf:
        outf.write(bound.export_cxx(namespace='mars_model_hrd_prior'))

    print(f'exporting initial guess to {model_file_name}.hpp')
    N = 1000
    ind = np.sort(np.random.choice(np.arange(len(X0)), N, replace=False))
    guess = InitialGuess(X0[ind], phot_columns, models)
    with open(model_file_name + '.hpp', 'a+') as outf:
        outf.write(guess.export_cxx(namespace="mars_model_guess"))

    print(f'exporting limits')
    with open(model_file_name + '.hpp', 'a+') as outf:
        outf.write(cxx_X_limits(X0, namespace='mars_model_limits'))

    print(f'plotting diagnostics to {model_file_name}.png')
    plot_diagnostics_residuals(models, ndf, pred_columns)

    return models, ndf, pred_columns
