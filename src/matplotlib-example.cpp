#define _USE_MATH_DEFINES // for sin/log
#include "matplotlibcpp.h"
#include <cmath>
#include <iostream>

namespace plt = matplotlibcpp;

void set_backend(){
  #if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
  #elif __APPLE__
      plt::backend("MacOSX");
  #elif __linux__
  #elif __unix__ // all unices not caught above
  #else
      #error "Unknown compiler"
  #endif
}

int main() {

  std::cout << "testing with matplotlib" << std::endl;
  set_backend();

  int n = 1000; // 5000 data points
  std::vector<double> x(n), y(n), z(n), w(n, 2);
  for (int i = 0; i < n; ++i) {
    x.at(i) = i * i;
    y.at(i) = sin(2 * M_PI * i / 360.0);
    z.at(i) = log(i);
  }

  plt::figure(); // declare a new figure (optional if only one is used)

  plt::plot(x, y);                        // automatic coloring: tab:blue
  plt::plot(x, w, "r--");                 // red dashed line
  plt::plot(x, z, {{"label", "log(x)"}}); // legend label "log(x)"

  plt::xlim(0, 1000 * 1000);    // x-axis interval: [0, 1e6]
  plt::title("Standard usage"); // set a title
  plt::legend();                // enable the legend
  // plt::savefig("standard.pdf"); // save the figure

  std::vector<std::vector<double>> xx, yy, zz;
    for (double i = -5; i <= 5;  i += 0.25) {
        std::vector<double> x_row, y_row, z_row;
        for (double j = -5; j <= 5; j += 0.25) {
            x_row.push_back(i);
            y_row.push_back(j);
            z_row.push_back(::std::sin(::std::hypot(i, j)));
        }
        xx.push_back(x_row);
        yy.push_back(y_row);
        zz.push_back(z_row);
    }

    plt::plot_surface(xx, yy, zz);
    plt::savefig("tmp.pdf");
    plt::show();
}
