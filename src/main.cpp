/**
 * Let's try to code some BOB emulator model in CPP
 */
#include <string>
#include <iostream>
#include <map>
#include <set>
#include <math.h>
#include <cmath>
#include <ctime>
#include <cstdio>
#include <sys/stat.h>
#include <algorithm>
// provided libs
#include <xtensor/xio.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xmath.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor/xindex_view.hpp>
#include <xtensor/xmanipulation.hpp>
// Bob libraries
#include <pbar.hpp>
#include <emcee.hpp>
#include <mfio.hpp>
#include <mfopts.hpp>
#include <bob/helpers.hpp>
#include <bob/polynomialregressionmodel.hpp>
#include <bob/emulator/piecewise.hpp>
#include <bob/star.hpp>
#include <bob/vaextable.hpp>
#include <bob/datacatalog.hpp>
#include <bob/distributions.hpp>
#include <bob/imf.hpp>
#include <bob/posterior.hpp>
#include <bob/reporter.hpp>
#include <bob/nanny.hpp>

typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;
using df_theta = std::map<std::string, double>;
using prior_fn = std::function<double(df_theta &)>;


/* TODO list
 * =========
 *
 * Done::
 *
 * In progress::
 *
 * Pending::
 *     [ ] check R0 if this would work too??
 *     [ ] Something is going on with the logM... possibly the IMF prior issue
 *
 *  BUG LIST
 *
 *  [ ] Export with io::AsciiWriter creates a first column but not in the header.
 */

namespace priors {

    /**
     * Un-normalized prior on A0 as function of dmod
     *
     * @param a0    extinction values
     * @param dmod  distance modulus values
     *
     * @return lnp(a0 | dmod)
     */
    DMatrix a0_given_dmod(const DMatrix& a0, const DMatrix& dmod){
        // 1pc, 10pc, 100pc, 1kpc, 4kpc, 10kpc, 60kpc
        DMatrix dmod_ref = {-5., 0, 5, 10, 13, 15, 19};
        DMatrix a0cut_ref = {-0.01, 0.01, 0.05, 0.1, 0.5, 2., 5.};

        DMatrix a0_cut = xt::interp(dmod, dmod_ref, a0cut_ref);

        // Below is a response function [1 / (1+p)]
        // the factor 5 comes from the 99% target value
        return 2. * (1 - 1. / (1 + xt::exp(- 5. * (a0 / a0_cut))));
    }

    /**
     * Extinction prior (independent from dmod)
     * @param a0 extinction value
     * @return lnp(a0)
     */
    double a0(const double a0){
        if ((a0 < -0.1) || (a0 > 20.)) {
            return -std::numeric_limits<double>::infinity();
        }
        return std::exp(-std::abs(a0));
    }

    /**
     * Extinction prior on R0 (independent from dmod)
     * @param r0 extinction value
     * @return lnp(r0)
     */
    double r0(const double r0){
        if ((r0 < 2.5) || (r0 > 3.5)) {
            return -std::numeric_limits<double>::infinity();
        }
        return std::exp(-std::pow(r0 - 3.1, 2) / 0.2);
    }

    /**
     * Un-normalized prior on A0 as function of dmod
     *
     * @param a0    extinction value
     * @param dmod  distance modulus value
     *
     * @return lnp(a0 | dmod)
     */
    double a0_given_dmod(const double a0, const double dmod){
        DMatrix a0_ = {a0};
        DMatrix dmod_ = {dmod};
        return a0_given_dmod(a0_, dmod_)[0];
    }

    /**
     * Photometric Jitter prior.
     *
     * @param logjit   log10 value of the jitter
     *
     * @return lnp(log10jit)
     */
    double log10jitter(const double logjit){
        if (logjit > -0.5) {   // jitter > 0.3
            return -std::numeric_limits<double>::infinity();
        }
        return std::exp(normal::logpdf(std::pow(10, logjit), 0, 1e-2));
    }

    /**
     * Distance Modulus prior.
     *
     * @param dmod  distance modulus value
     *
     * @return lnp(dmod)
     */
    double dmod(const double mu){
        if ((mu < -5) || (mu > 19)) {
            return -std::numeric_limits<double>::infinity();
        }
        return 1.;
    }

    /**
     * log(age/yr) prior
     *
     * @param logage  log10 age value
     *
     * @return lnp(logage)
     */
    double logage(const double logA){
        if ((logA < 6) || (logA > 13)) {
            return -std::numeric_limits<double>::infinity();
        }
        return 1.;
    }
};


/**
 * Guess the APs based on sed given a distance.
 *
 * The initial guess depends on how many dimensions we sample.  This is some
 * time to find a guess that works for every star.  Hence, this has to be done
 * on a star by star basis, but because of the parallax/distance parameter, it
 * requires some optimisation unless one provides a distance.
 *
 * In this version, we fix the distance modulus (given) and compute for each
 * model in the emulator, which has the best posterior value.
 *
 * Models from the emulator are identified by there anchor points (center of
 * bins) in sampled parameter space (e.g, logT, logL, A0, ...). For each, we
 * compute the posterior in the exact manner used during the sampling (which
 * includes priors etc).
 *
 * The guess includes the given dmod value and -1 as log10jitter.
 *
 * @param star  Star object to guess properties
 * @param lnp   Posterior function
 * @param dmod  distance modulus value
 *
 * @return guess of the theta to be used to start sampling the posterior.
 */
std::vector<double> get_guess_given_dmod(
        const Star& star,
        const Emulator::PieceWise emulator,
        Posterior& lnp,
        const double dmod) {

    auto ind = xt::isfinite(star.sed_values) && (xt::isfinite(star.sed_error));

    std::string best_model_key;
    double score = -1e30;

    auto models = emulator.models;
    if (models.empty()){
        throw std::runtime_error("Models need to be set first");
    }

    size_t ndim = lnp.names.size();
    std::vector<double> theta(ndim);
    theta[ndim - 2] = dmod;
    theta[ndim - 1] = -5;

    /*
    std::cout << "DEBUG: guess: ";
    for (auto val: lnp.names){ std::cout << val << " ";}
    std::cout << " | " << "score" << "\n";
    */

    for (auto key_model_pair : models){
        auto model = key_model_pair.second;
        DMatrix tmp_aps = (model.x_min + model.x_max) * 0.5;

        for (size_t i=0; i< ndim - 2; ++i){theta[i] = tmp_aps[i]; }

        double score_ = lnp.lnposterior(xt::adapt(theta, {theta.size()}), star);

        if (score_ > score){
            best_model_key = key_model_pair.first;
            score = score_;
            /*
            std::cout << "DEBUG: guess: ";
            for (auto val: theta){ std::cout << val << " ";}
            std::cout << " | " << score_ << "\n";
            */
        }
    }

    auto tmp = models[best_model_key].x_min + models[best_model_key].x_max;
    std::vector<double> guess;
    for (double val: tmp){
        guess.push_back(0.5 * val);
    }
    guess.push_back(theta[ndim-2]);
    guess.push_back(theta[ndim-1]);   // log10jitter
    return guess;
}



/**
 * Where the work happens
 *
 * @param catalog_filename       input catalog filename and path
 * @param emulator_filename      input emulator source filename and path
 * @param star_index_min         catalog processing start index
 * @param star_index_max         catalog processing end slice (processing < max)
 * @param nwalkers               number of Goodman & Weare walkers
 * @param nburn                  number of burning steps
 * @param nkeep                  number of steps to keep as final
 * @param catalog_output_prefix  prefix of the catalog output file
 * @param samples_output_prefix  prefix of the sampling output file per star
 *
 * @return status code
 */
int main_process(
         const std::string & catalog_filename,
         const std::string & emulator_filename,
         size_t star_index_min,
         size_t star_index_max,
         const size_t nwalkers = 40,
         const size_t nburn = 2000,
         const size_t nkeep = 500,
         const std::string & catalog_output_prefix = "catalog",
         const std::string & samples_output_prefix = "star_samples",
         const bool export_samples = false
        ) {

    // Showing some info for the run
    std::string catalog_output = catalog_output_prefix
                                  + string_sprintf("_%d_%d.csv",
                                                   star_index_min, star_index_max);


    std::cout << "Input configuration\n"
              << "--------------------------------\n"
              << "\nInput:\n"
              << "          catalog file: " << catalog_filename  << "\n"
              << "      processing slice: " << star_index_min << " - " << star_index_max << "\n"
              << "\nModels:\n"
              << "         emulator from: " << emulator_filename << "\n"
              << "\nOutput:\n"
              << "   catalog output file: " << catalog_output << "\n";
    if (export_samples)
        std::cout << "        output samples: " << samples_output_prefix + "_{star.name}.csv\n";
    std::cout << "\nEmcee parameters:\n"
              << "              nwalkers: " << nwalkers << "\n"
              << "         burning steps: " << nburn << "\n"
              << "        sampling steps: " << nkeep << "\n";


    // Define mapping ================================
    xt::xarray<std::string> mapping = {
            {"name"              , "source_id" , "-"}              ,
            {"parallax"          , "parallax"  , "parallax_error"} ,
            {"Gaia_MAW_BP_faint" , "bpmag"     , "bpmag_error"}    ,
            {"Gaia_MAW_G"        , "gmag"      , "gmag_error"}     ,
            {"Gaia_MAW_RP"       , "rpmag"     , "rpmag_error"}    ,
            {"2MASS_J"           , "j_m"       , "j_msigcom"}      ,
            {"2MASS_H"           , "h_m"       , "h_msigcom"}      ,
            {"2MASS_Ks"          , "ks_m"      , "ks_msigcom"}     ,
            {"WISE_RSR_W1"       , "w1mpro"    , "w1mpro_error"}   ,
            {"WISE_RSR_W2"       , "w2mpro"    , "w2mpro_error"}
    };

    DataCatalog cat(catalog_filename, mapping);
    std::cout << cat << std::endl;
    std::cout << "n_objects: " << cat.size() << "\n";

    // load model

    std::cout << "Emulator object " << emulator_filename << "\n"
              << "--------------------------------\n";

    Emulator::PieceWise emulator = Emulator::emulator_from_single_hdf5(emulator_filename);
    std::cout << emulator << std::endl;

    // Define priors ============================
    IMF::Kroupa2001 imf;

    // Note that in the following we use df.at
    // df.at throws a out_of_range exception if not existing
    // while df[] creates a null element instead without exception.
    std::vector<prior_fn> gaia_priors({
            [](df_theta &df) { return priors::dmod(df.at("dmod")); },
            [](df_theta &df) { return priors::logage(df.at("logA")); },
            [](df_theta &df) { return priors::a0(df.at("A0")); },
            [](df_theta &df) { return priors::r0(df.at("R0")); },
            [](df_theta &df) { return priors::log10jitter(df.at("log10jitter")); },
            // [](df_theta &df) { return priors::a0_given_dmod(df.at("A0"), df.at("dmod")); },
            [imf](df_theta &df) { return imf.get_value(std::pow(10, df.at("logM"))); }
            });

    xt::xarray<std::string> prior_names = {"dmod", "logage", "A0", "R0",
                                           "log10Jitter",
                                           // "A0|dmod",
                                           "logM"
                                            };

    auto phot_columns = xt::view(mapping, xt::range(2, _), 0);
    Posterior lnp(&emulator, phot_columns);
    lnp.set_priors(gaia_priors);
//    std::cout<< lnp << std::endl;


    std::cout << "\nLn-posterior\n"
              << "================================\n";

    std::cout << " Using the following priors: ";
    for (const auto& name : prior_names){
        std::cout << name << ", ";
    }
    std::cout << std::endl;

    io::AsciiWriter catalog_wt(catalog_output);
    catalog_wt.set_delimiter(",");
    catalog_wt.set_comment("#");

    // make sure slice is properly set
    if (star_index_min < 0) { star_index_min = 0; }
    if (star_index_min >= star_index_max){
        star_index_max = star_index_min + 1;
    }


    // which fields should be exported by the reporter
    // std::vector<std::string> which = {"logg", "logA", "Mbol"};
    std::vector<std::string> which = {"logg", "logA", "logM"};
    for (const auto& name : phot_columns){
        which.push_back(name);
    }
    for (const auto& name : phot_columns){
        which.push_back("A_" + name);
    }

    std::cout << "Reporting on " << which.size() << " quantities\n     ";
    for (const auto& name : which){ std::cout << name << " "; }
    std::cout << std::endl;

    std::cout << "\nProcessing data inference...\n"
              << "================================\n";

    star_index_max = std::max<size_t>(0, std::min<size_t>(star_index_max, cat.size()));
    star_index_min = std::max<size_t>(0, std::min<size_t>(star_index_min, cat.size()));
    if ((star_index_max - star_index_min) < 1){
        std::cout << "Empty data slice: "
                  << "the input catalog contains only " << cat.size() << " objects.\n";
        return 0;
    }

    PBar global_process_status(star_index_max - star_index_min);
    global_process_status.set_description("Overall progress");
    for(size_t star_index = star_index_min; star_index < star_index_max;
            ++star_index, ++global_process_status){

        int attempt = 0;
        int n_attempt_max = 1;
        int retval = 1;

        // collisions when reading hdf input data... protecting and trying to
        // retry
        while((attempt < n_attempt_max) && (retval > 0)){
            try{
                Star star = cat.get_star(star_index);
                Star recalibrated_star = star.get_recalibrated();

                // show information
                std::cout << recalibrated_star;

                double dmod_guess = 5 * std::log10(1e2/star.parallax);
                // Let's make sure we start in allowed regimes (i.e., in MW)
                if (dmod_guess > 18) {dmod_guess = 18.0;}

                // Star guess ========================
                /*  Guess for parsec models
                std::vector<double> guess = {0.01, -0.8199871632291023, 2.731916974815796,
                                             3.7834331184247643, 0.4284359003062165,
                                             dmod_guess, -1.0};
                */

                // Guess for parsec models for parsec_finer:
                // A0, M/H, logL, logT, logM, logA, dmod, log10jitter
                // std::vector<double> guess = {0.1, 0.0, 1.0, 3.783, 0, 8., dmod_guess, -1.0};
                std::vector<double> guess = get_guess_given_dmod(star, emulator, lnp, dmod_guess);

                // guess old orion grid
                // A0, logL, logT, R0, dmod, log10jitter
                // std::vector<double> guess = {0.1, 1.0, 3.783, 3.1, dmod_guess, -1.0};

                // Show theta
                std::cout << "Guess for Star: " << star.name << "\n"
                          << "---------------------------------\n";

                size_t lenkey = 0;
                for (const auto & item: lnp.names){
                    lenkey = std::max(lenkey, item.size());
                }
                for (int i = 0; i < guess.size(); ++i){
                    std::cout << std::setw(lenkey) << lnp.names(i) << " =  " << guess[i] << "\n";
                }
                std::cout << std::endl;
                DMatrix theta = xt::adapt(guess, {guess.size()});
                std::cout << "    lnlikelihod: " << lnp.lnlikelihood(theta, recalibrated_star) << '\n';
                std::cout << "        lnprior: " << lnp.lnprior(theta) << '\n';
                std::cout << "    lnposterior: " << lnp.lnposterior(theta, recalibrated_star) << '\n';
                std::cout << std::endl;

                // Checks on lnp(theta) and lnp(data|theta)
                // Print out diagnostics if something goes wrong.
                if (!std::isfinite(lnp.lnprior(theta))){
                    df_theta df = lnp.theta_to_map(theta);
                    std::cout << df << std::endl;
                    for (size_t k=0; k < lnp.priors.size(); ++k){
                        std::cout << "DEBUG: lnp(" << prior_names[k] << ") = " << lnp.priors[k](df) << "\n";
                    }
                    throw std::runtime_error("Initial guess is invalid: the guess log-prior is not finite");
                }
                if (!isfinite(lnp.lnlikelihood(theta, recalibrated_star))){
                    throw std::runtime_error("Initial guess is invalid: the guess log-likelihood is not finite");
                }


                // EMCEE =============================
                size_t nsteps = nburn + nkeep;
                /* number of dimensions in parameter space */
                size_t ndim = theta.shape(0);


                std::cout << "Emcee parameters\n"
                          << "--------------------------------\n"
                          << "   nwalkers: " << nwalkers  << "\n"
                          << "      nburn: " << nburn  << "\n"
                          << "      nkeep: " << nkeep  << "\n"
                          << "       ndim: " << ndim << "\n"
                          << "   sampling: ";
                for(const auto& name : lnp.names){
                    std::cout << name << ", ";
                }
                std::cout << std::endl << std::endl;


                io::AsciiWriter samples_wt;
                io::AsciiWriter input_wt;
                if (export_samples) {
                    std::string star_output = samples_output_prefix + "_" + star.name + "_input.csv";
                    input_wt = io::AsciiWriter(star_output);
                    input_wt.set_delimiter(",");
                    input_wt.set_comment("#");
                    input_wt << "name"
                             << "recalibrated"
                             << "parallax"
                             << "parallax_unc";
                    for (const auto& name: star.sed_names){
                        input_wt << name
                                 << name + "_unc";
                    }
                    ++input_wt;

                    input_wt << star.name
                             << star.is_recalibrated()
                             << star.parallax
                             << star.parallax_error;
                    for (size_t i = 0; i < star.sed_names.shape(0); ++i){
                        input_wt << star.sed_values[i]
                                 << star.sed_error[i];
                    }

                    std::string samples_output = samples_output_prefix + "_" + star.name + "_samples.csv";
                    samples_wt = io::AsciiWriter(samples_output);
                    samples_wt.set_delimiter(",");
                    samples_wt.set_comment("#");
                    samples_wt << "chain";
                    for (const auto& name: lnp.names){
                        samples_wt << name;
                    }
                    for (const auto& name: star.sed_names){
                        samples_wt << name;
                    }
                    samples_wt << "lnp";
                    ++samples_wt;
                }

                // define function to optimize
                std::function<double (std::vector<double>&)> lnpfunc;
                auto partial_lnp = std::bind(lnp_wrapper,
                        std::placeholders::_1, lnp, recalibrated_star);
                lnpfunc = partial_lnp;

                // prepare the walkers
                std::vector<std::vector<double> > walkers;
                std::vector<double> lnp_values(nwalkers);

                emcee::init_walkers_from_ball(nwalkers, walkers, guess, 1e-3);
                for (size_t k = 0; k < nwalkers; ++k) { lnp_values[k] = lnpfunc(walkers[k]); }

                // init. accept vector
                std::vector<bool> accept(nwalkers);
                for (size_t k = 0; k < nwalkers; ++k) { accept[k] = false; }

                // run a production chain
                PBar pb(nsteps);
                pb.set_description("burn 1/2");
                for(size_t step=0; step < nburn / 2; ++step, ++pb){
                    emcee::step(walkers, lnp_values, accept, lnpfunc);
                }

                size_t argmax = 0;
                double lnp_max = 0;
                for (size_t k = 0; k < nwalkers; ++k){
                    if (lnp_values[k] > lnp_max){
                        lnp_max = lnp_values[k];
                        argmax = k;
                    }
                }

                pb.set_description("burn 2/2");
                guess = walkers[argmax];
                std::vector<std::vector<double> > walkers1;
                emcee::init_walkers_from_ball(nwalkers, walkers1, guess, 1e-3);
                for (size_t k = 0; k < nwalkers; ++k) {
                    lnp_values[k] = lnpfunc(walkers1[k]);
                    if (~std::isfinite(lnp_values[k])){
                        std::cout << "chain " << k << " lnp inf\n";
                    }
                }
                for (size_t k = 0; k < nwalkers; ++k) { accept[k] = false; }
                for(size_t step=0; step < nburn / 2; ++step, ++pb){
                    emcee::step(walkers1, lnp_values, accept, lnpfunc);
                }

                pb.set_description("sampling");
                std::vector<std::vector<double>> keep_samples;
                std::vector<double> keep_lnp_values;
                for(size_t step=0; step < nkeep; ++step, ++pb){
                    emcee::step(walkers1, lnp_values, accept, lnpfunc);
                    // convenient cout shortcut.
                    if (export_samples){
                        //DEBUG TODO: Adding sed prediction in the samples
                        DMatrix predicted_seds;
                        for(size_t k=0; k < nwalkers; ++k, ++samples_wt){
                            samples_wt << k;
                            for (size_t dim = 0; dim < ndim; ++dim)
                                samples_wt << walkers1[k][dim];
                            //DEBUG TODO: Adding sed prediction in the samples
                            predicted_seds = lnp.predict_observed_sed(walkers1[k]);
                            for (size_t dim = 0; dim < predicted_seds.shape(0); ++dim)
                                samples_wt << predicted_seds(k, dim);
                            samples_wt << lnp_values[k];
                        }
                    }
                    for (auto & val: walkers1){
                        keep_samples.push_back(val);
                    }
                    for (auto & val: lnp_values){
                        keep_lnp_values.push_back(val);
                    }
                }
                pb.finish();

                std::cout << std::endl;

                Reporter reporter(star, lnp, keep_samples, which);
                std::cout << reporter << std::endl;

                reporter.to_csv(catalog_wt, (star_index == star_index_min));
                retval = 0;
            } catch (const std::exception& e){
                std::cout << e.what() << std::endl;
                attempt++;
                if (attempt < n_attempt_max){
                    std::cout << "attempting to restart star " << star_index << ". Try number: " << attempt << "\n";
                } else {
                    std::cout << "Star " << star_index << "failed.\n";
                }
            }
        }
    }
    global_process_status.finish();

    return 0;
}

/**
 * Defines a string based on the current time.
 */
std::string get_default_name(const std::string& prefix=""){
     // current date/time based on current system
    time_t now = time(0);
    tm *ltm = localtime(&now);
    return prefix + string_sprintf("%4d%02d%02d_%02d%02d%02d",
                                   1900 + ltm->tm_year,
                                   1+ltm->tm_mon,
                                   ltm->tm_mday,
                                   1 + ltm->tm_hour,
                                   1 + ltm->tm_min,
                                   1 + ltm->tm_sec);
}


int main(int argc, char * argv[]){

    mfopts::Options opt(argv[0]);
     opt.add_option("-h,--help",           "display help message");
     opt.add_option("-m,--model_file",     "emulator model file");
     opt.add_option("-i,--input_catalog",  "input data catalog ");
     opt.add_option("-r,--run_name",       "Name of the run for logs");
     opt.add_option("-o,--output",         "catalog output path and prefix");
     opt.add_option("-O,--samples",        "output samples to path and prefix if set");
     opt.add_option("--from",              "catalog processing starts at this index");
     opt.add_option("--to",                "catalog processing stops  before this index");
     opt.add_option("--nwalkers",          "number of Goodman & Weare walkers");
     opt.add_option("--nburn",             "number of burning steps");
     opt.add_option("--nkeep",             "number of steps to keep as final");

     opt.parse_options(argc, argv);

     if (opt.has_option("-h")){
         std::cout<< opt.help();
         exit(0);
     }

    /*
     * Default parameters
     * ==================
     */

    // input data
    std::string catalog_filename("vst_lbol-result.vaex.hdf5");
    std::string emulator_filename("emulators/parsec.emulator.hdf5");
    std::string runname(get_default_name("cbob_run_"));

    // processing slice
    size_t star_index_min = 0;
    size_t star_index_max = 10;

    // Emcee
    size_t nwalkers = 40;   //number of Goodman & Weare walkers
    size_t nburn = 2000;    // number of burning steps
    size_t nkeep = 500;     // number of steps to keep as final

    // outputs
    std::string catalog_output_prefix = runname + "_catalog";
    std::string samples_output_prefix = runname + "_star_samples";
    bool export_samples = false;

    // update defaults based on command-line
    if (opt.has_option("--run_name")){
        runname = opt.get_option<std::string>("--run_name");
        catalog_output_prefix = runname + "_catalog";
        samples_output_prefix = runname + "_star_samples";
    }
    if (opt.has_option("--input_catalog")){
        catalog_filename = opt.get_option<std::string>("--input_catalog");
    }
    if (opt.has_option("--model_file")){
        emulator_filename = opt.get_option<std::string>("--model_file");
    }
    if (opt.has_option("--output")){
        catalog_output_prefix = opt.get_option<std::string>("--output");
    }
    if (opt.has_option("--samples")){
        export_samples = true;
        samples_output_prefix = opt.get_option<std::string>("--samples");
    }
    if (opt.has_option("--from")){
        star_index_min = opt.get_option<size_t>("--from");
    }
    if (opt.has_option("--to")){
        star_index_max = opt.get_option<size_t>("--to");
    }
    if (opt.has_option("--nwalkers")){
        nwalkers = opt.get_option<size_t>("--nwalkers");
    }
    if (opt.has_option("--nburn")){
        nburn = opt.get_option<size_t>("--nburn");
    }
    if (opt.has_option("--nkeep")){
        nkeep = opt.get_option<size_t>("--nkeep");
    }

    auto basename = split_string(catalog_filename, "/");

    std::string run_reference_name(
            runname + "_" + basename[basename.size() - 1] + "_" +
            string_sprintf("%d_%d", star_index_min, star_index_max)
            );

    std::cout << "\nRun reference name " << run_reference_name << std::endl;
    Nanny nanny(run_reference_name);
    nanny.lock_task();

    int attempt = 0;
    int retval = 1;
    int n_max_attempts = 5;

    while((attempt < n_max_attempts) && (retval > 0)){
        try{
            retval = main_process(catalog_filename, emulator_filename,
                                  star_index_min, star_index_max,
                                  nwalkers, nburn, nkeep,
                                  catalog_output_prefix,
                                  samples_output_prefix,
                                  export_samples);
        } catch (const std::exception& e){
            std::cout << e.what() << std::endl;
            attempt++;
            std::cout << "attempting to restart whole slice. Try number: " << attempt << "\n";
        }
    }

    nanny.release_task();
    return retval;
}

// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
