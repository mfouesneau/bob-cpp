/**
 * A small code to test the models and residuals of the training
 */
#include <string>
#include <iostream>
#include <math.h>
#include <cmath>
#include <cstdio>
#include <vector>
#include "xtensor/xarray.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xview.hpp"
#include "xtensor/xadapt.hpp"
#include "bob/emulator/base.hpp"
#include "bob/mars_model/emulator.hpp"
#include "bob/mars_model/initial_guess.hpp"
#include "bob/mars_model/boundary2d.hpp"
#include "mfio.hpp"
#include "pbar.hpp"


/* TODO list
 *
 * [x] add boundaries to extrapolation zone: Boundary2D
 * [ ] see how feature importance is calculated or store them
 *
 */
using shape_type = std::vector<std::size_t>;

int main(int argc, char* argv[]){

    // std::vector<std::string> xnames = {A0, logT, logL, R0};
    std::vector<double> x = {0., 4.46899, 4.09635, 3.1};

    // emulator
    auto emulator = Emulator::Mars();
    std::cout << emulator << std::endl;
    auto pred = emulator.predict(x);
    size_t n_labels = pred.size();
    for (size_t i = 0; i < n_labels; ++i){
	    std::cout << mars_model::label_names[i] << " = " << pred[i] << std::endl;
    }
    // boundary conditions
    Boundary2D bound(mars_model_hrd_prior::mask,
                     mars_model_hrd_prior::x_coords,
                     mars_model_hrd_prior::y_coords);
    std::cout << std::endl << "valid: " << bound.predict(x[1], x[2]) << std::endl << std::endl;

    xt::xarray<std::string> phot_columns = {"Gaia_MAW_G", "Gaia_MAW_BP_faint", "Gaia_MAW_RP",
                                            "2MASS_H", "2MASS_J", "2MASS_Ks",
                                            "WISE_RSR_W1", "WISE_RSR_W2"};
    InitialGuess guess(phot_columns);

    std::cout << guess << std::endl;

    std::vector<double> sed = {1.58371, 8.77999, -0.304794, -7.07934, -5.09084, -8.05206, -8.68346, -9.22764};
    std::cout << guess.predict(sed, sed, 0.) << "\n";

    io::AsciiReader rd("./temp/param_PHOENIX_NOM1_G150.dat");
    rd.set_comment("#");

    // warning: the following does not keep the requested ordering
    // but keeps the data file order of the columns
    std::vector<std::string> columns = {"A0", "teff", "logL", "R0"};
    rd.set_columns_from_names(columns);

    // rd.set_columns({5, 6, 24, 29});
    std::cout << rd.get_header() << "\n";

    double a0, logL, teff, r0;

    io::AsciiWriter wt("./temp/param_PHOENIX_NOM1_G150_predictions.csv");
    wt.set_delimiter(",");
    wt.set_comment("");
    wt << wt.get_comment();
    for (auto val: emulator.names()){
        wt << val;
    }
    for (auto val: emulator.prediction_names()){
        wt << val;
    }
    wt << "valid" ;
    ++wt;

    PBar pb(rd.count_lines());
    pb.set_description("processing");
    while(++rd){
        rd.get_next_value<double>(r0);    // 5
        rd.get_next_value<double>(a0);    // 6
        rd.get_next_value<double>(logL);  // 24
        rd.get_next_value<double>(teff);  // 29
        std::vector<double> xin = {a0, std::log10(teff), logL, r0};
        auto pred = emulator.predict(xin);
        auto valid = bound.predict(std::log10(teff), logL);
        wt << xin;
        wt << pred << valid;
        ++wt;
        ++pb;
    }
    return 0;
}
