/**
 * Concatenates many ascii outputs into a single file.
 *
 * Assumes
 *    * every file is structurally consistent with the others. (no checks)
 *    * files only contain a 1 line for the header
 *
 * Returns the content of all files found without their headers (only the first
 * header is kept. Ordering is not imposed in the outputs.
 */
#include <string>
#include <iostream>
#include <list>
#include <fstream>
// Bob libraries
#include <pbar.hpp>
#include <bob/helpers.hpp>
#include <mfio.hpp>
#include <cmath>
#include <highfive/H5File.hpp>
#include <highfive/H5Easy.hpp>

// using namespace std::string_literals;

/**
 * Nice representation of Catalog objects with cout
 */
template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T>& vec) {
    for (const auto val: vec){
        os << val << "\t";
    }
    os << std::endl;
    return os;
}

/**
 * @brief Concatenate vectors
 *
 * @tparam T       type of the data
 * @param orig     first vector
 * @param other    other vector
 * @param inplace  set to append other to orig data
 * @return std::vector<T> the created vector (or updated orig)
 */
template<typename T>
std::vector<T> append(std::vector<T>& orig, std::vector<T>& other, bool inplace=false){

    std::vector<T> v;
    if (inplace){
        v = orig;
    }
    v.reserve(v.size() + other.size());
    v.insert(v.end(), other.begin(), other.end());
    return v;
}

/**
 * @brief Concatenate a list of vectors
 *
 * @tparam T        data type of the vectors
 * @param vectors   vectors to concatenate
 * @return std::vector<T>  the concatenated vector
 */
template<typename T>
std::vector<T> concatenate(const std::list<std::vector<T>>& vectors){

    size_t total = 0;
    for (const auto & vec: vectors){
        total += vec.size();
    }
    std::vector<T> v;
    v.reserve(total);
    size_t offset = 0;
    for (const auto & vec: vectors){
        v.insert(v.begin() + offset, vec.begin(), vec.end());
        offset += vec.size();
    }
    return v;
}

/**
 * @brief A small class to read csv results and offer ways to concatenate and export results.
 *
 * This class also takes care of the '\x00' issues in the csv outputs.
 */
class ResultDataFrame{

    public:
        std::vector<std::vector<float>> f_data;   // all single precision float fields
        std::vector<std::string> s_sourceid;      // sourceid string values only
        std::vector<std::string> s_fields;        // all field names [sourceid, ...]

        size_t size(){ return this->s_sourceid.size();};

        ResultDataFrame(const std::vector<std::string>& s_fields,
                        const std::vector<std::string>& v_sourceid,
                        const std::vector<std::vector<float>>& v_data);
        ResultDataFrame(const std::string& fname);
        void export_vaex(const std::string & fname,
                         unsigned mode=H5Easy::File::ReadWrite | H5Easy::File::Create,
                         bool progress=false);
};

/**
 * @brief Construct a new Result Data Frame:: Result Data Frame object
 *
 * @param s_fields     all field names
 * @param v_sourceid   sourceid (string) vector
 * @param v_data       all float fields (i.e, everything else)
 */
ResultDataFrame::ResultDataFrame(
                    const std::vector<std::string>& s_fields,
                    const std::vector<std::string>& v_sourceid,
                    const std::vector<std::vector<float>>& v_data):
                    s_fields(s_fields),
                    s_sourceid(v_sourceid),
                    f_data(v_data){ }

/**
 * @brief Construct a new Result Data Frame from csv file.
 *
 * @param fname data to read in
 */
ResultDataFrame::ResultDataFrame(const std::string& fname){

    // count how many entries
    size_t n_entries = io::count_lines(fname, "#") - 1;  // remove headers

    // setup the reader
    io::AsciiReader rd = io::AsciiReader(fname);
    rd.set_delimiter(",");
    rd.set_comment("#");

    size_t n_fields = rd.get_number_of_fields();

    this->s_sourceid.resize(n_entries);
    for (const auto & name: rd.get_header()){ this->s_fields.push_back(name);}
    for (size_t k=0; k < n_fields-1; ++k){ this->f_data.push_back(std::vector<float>(n_entries));}

    //buffers
    std::string sourceid = "";
    float value = 0.;
    size_t index = 0;
    ++rd;                  // remove header line
    // Read the file
    while(++rd){
        rd.get_next_value<std::string>(sourceid);
        remove_nulls(sourceid);   // clean up \x00
        this->s_sourceid[index] = sourceid;
        for (size_t k=0; k < n_fields - 1; ++k){
            rd.get_next_value<float>(value);
            this->f_data[k][index] = value;
        }
        ++index;
    }
}

/**
 * @brief Export result into a vaex formatted hdf5
 *
 * @param fname filename for the exported data
 * @param mode  H5Easy::File::ReadWrite | H5Easy::File::Create | H5Easy::File::Overwrite
 * @param progress set to show a progress indicator
 */
void ResultDataFrame::export_vaex(const std::string& fname, unsigned mode, bool progress){

    H5Easy::File file(fname, mode);

    std::string field_name = "source_id";
    std::string where = "/table/columns/source_id/data";
    std::vector<size_t> sourceid(this->s_sourceid.size());

    PBar pb_export(this->f_data.size() + 1);
    if (progress) pb_export.set_description("Exporting");

    for (size_t k=0; k < sourceid.size(); ++k){
        sscanf(this->s_sourceid[k].c_str(), "%zu", &(sourceid[k]));
    }
    if (progress) ++pb_export;

    // kind of bug that the dump does not read the file mode
    enum H5Easy::DumpMode dumpmode;
    if (mode == H5Easy::File::Overwrite){
        dumpmode = H5Easy::DumpMode::Overwrite;
    } else  {
        dumpmode = H5Easy::DumpMode::Create;
    }


    H5Easy::dump(file, where, sourceid, dumpmode);
    for (size_t k=0; k < this->f_data.size(); ++k){
        if (progress) ++pb_export;
        field_name = this->s_fields[k + 1];   // k = 0 is sourceid
        std::string where = "/table/columns/" + field_name + "/data";
        H5Easy::dump(file, where, this->f_data[k], dumpmode);
    }
    if (progress) pb_export.finish();
}


/**
 * @brief pretty print the ResultDataFrame object
 *
 * @param os    stream to put the representation on
 * @param res   dataframe to represent
 * @return std::ostream&  stream
 */
std::ostream &operator<<(std::ostream &os, const ResultDataFrame res) {
    os << "ResultDataFrame: length=" << res.s_sourceid.size()
              << " x columns=" << res.s_fields.size()
              << "\n" ;
    size_t n_display = std::min(static_cast<size_t>(5), res.s_sourceid.size());

    os << "source_id: ";
    for (size_t k = 0; k < n_display; ++k){
        os << res.s_sourceid[k] << " ";
    }
    os << "...\n";

    for (size_t field_k=0; field_k < res.f_data.size(); ++field_k){
        os << res.s_fields[field_k + 1] << ": ";
        for (size_t k = 0; k < n_display; ++k){
            os << res.f_data[field_k][k] << " ";
        }
        os << "...\n";
    }
    os << std::endl;
    return os;
}


/**
 * @brief Concatenate many dataframes
 *
 * @param dfs  the data to combine
 * @return ResultDataFrame  combined data
 */
ResultDataFrame concatenate(std::list<ResultDataFrame> dfs){
    std::vector<std::vector<float>> v_data;
    std::list<std::vector<float>> f_vect;
    std::list<std::vector<std::string>> s_vect;

    // get all sourceids
    for (const auto& df: dfs){
        s_vect.push_back(df.s_sourceid);
    }

    ResultDataFrame res = dfs.front();

    // get all data per field
    for (size_t k = 0; k < res.f_data.size(); ++k){
        for (const auto& df: dfs){
            f_vect.push_back(df.f_data[k]);
        }
        v_data.push_back(concatenate(f_vect));
        f_vect.clear();
    }

    ResultDataFrame newres(res.s_fields,
                           concatenate<std::string>(s_vect),
                           v_data);

    return newres;
}


std::vector<std::string> convert(
        const std::vector<std::string>& filenames,
        const std::string& outputfname,
        const unsigned mode=H5Easy::File::ReadWrite | H5Easy::File::Create,
        size_t multifiles=0,
        size_t n_entry_max=10000000){

    std::list<ResultDataFrame> dfs;

    size_t n_files = filenames.size();
    size_t n_entries = 0;
    size_t n_processed = 0;
    bool stop = false;

    PBar pb_readin(n_files);
    pb_readin.set_description("Loading data");
    for (size_t k = 0; (k < n_files) & !stop; ++k, ++pb_readin){
        if (io::count_lines(filenames[k], "#") > 0){
            ResultDataFrame df(filenames[k]);
            dfs.push_back(df);
            n_entries += df.size();
        }
        pb_readin.set_description("Loading (current=" + std::to_string(n_entries) + ")");
        n_processed++;
        if (n_entries >= n_entry_max){
            std::cout << "\nReached the max. number of entries per file(n_entry_max="
                    << n_entry_max << ").\n Multiple output files will be created if necessary.";
            stop = true;
        }
    }
    pb_readin.finish();
    const std::vector<std::string> sublist(filenames.begin() + n_processed, filenames.end());

    if ((stop & (n_files > 1)) || ((multifiles > 0) & (n_entries > 0))) {
        auto tmp = split_string(outputfname, ".");
        tmp[tmp.size() - 1] = "_" + std::to_string(multifiles) + "." + tmp[tmp.size() - 1];
        std::string final_name;
        for (const auto &piece : tmp) final_name += piece;
        concatenate(dfs).export_vaex(final_name, mode, true);
        std::cout << "Chunk #" << multifiles + 1 << " created into " << final_name << "\n";
        std::cout << "       containing " << n_entries << " entries." << "\n";
    } else {
        concatenate(dfs).export_vaex(outputfname, mode, true);
        std::cout << "Data exported into " << outputfname << "\n";
        std::cout << "       containing " << n_entries << " entries." << "\n";
    }
    // run the next batch if necessary
    return sublist;
}


int main(int argc, char * argv[]){

    //std::string current_exec_name = argv[0]; // Name of the current exec program

    std::string input_path = "results";
    std::string input_pattern = ".*.csv";
    std::string outputfilename = "example.h5";

    if (argc > 3) {
        input_path = argv[1];
        input_pattern = argv[2];
        outputfilename = argv[3];
    } else {
        std::cout << "Wrong number of parameters or argmuments\n"
                  << "USAGE: " << argv[0]
                  << " <directory> <pattern> <outputfile>\n"
                  << "\n"
                  << "Example: " << argv[0] << " results '.*.csv' catalog.vaex.hdf5\n"
                  << "\n";
        return 1;
    }

    std::vector<std::string> list0 = list_dir(input_path, input_pattern);
    std::cout << "Found " << list0.size() << " files to combine.\n";
    if (list0.size() < 1){
        std::cerr << "No input data found with pattern "
                  << input_path << "/" << input_pattern << "\n";
        return 1;
    }

    unsigned mode = H5Easy::File::Overwrite;
    size_t multifiles = 0;
    size_t n_entry_max = 1000000;
    std::vector<std::string> sublist = convert(list0, outputfilename, mode, multifiles, n_entry_max);
    while (sublist.size() > 0){
        multifiles++;
        sublist = convert(sublist, outputfilename, mode, multifiles, n_entry_max);
    }

    std::cout << "Done.\n";
    return 0;
}
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
