#include <string>
#include <iostream>
// #include <cstdio>
#include <fstream>
// Bob libraries
#include <pbar.hpp>
#include <bob/helpers.hpp>
#include <mfio.hpp>
#include <locale>
#include <stdio.h>
#include <thread>
#include <threadpool.hpp>

size_t count(const std::string& fname){
    FILE *fp;
    errno = 0;
    fp = fopen(fname.c_str(), "r");
    size_t lines = 0;

    if ( fp == NULL ) {
        std::cout << "Error with " << fname
                  << ". fp is NULL (errno = " << errno << ")\n";
        return -1;
    }

    while (EOF != (fscanf(fp, "%*[^\n]"), fscanf(fp,"%*c")))
        ++lines;
    fclose(fp);
    return lines;
}

int main(int argc, char * argv[]) {
    std::string input_path = "results";
    std::string input_pattern = ".*.csv";
    std::string outputfilename = "catalog.csv";
    bool parallel = false;

    if (argc > 2) {
        input_path = argv[1];
        input_pattern = argv[2];
        if (argc > 3)
            parallel = (std::string(argv[3]).compare("--parallel") == 0);
    } else {
        std::cout << "Wrong number of parameters or argmuments\n"
                  << "USAGE: " << argv[0]
                  << " <directory> <pattern> [--parallel]\n"
                  << "\n"
                  << "Example: " << argv[0] << " results '.*.csv' catalog.csv --parallel\n"
                  << "\n";
        return 1;
    }

    // set cout using clean integers
    std::cout.imbue(std::locale(""));
    std::vector<std::string> list0 = list_dir(input_path, input_pattern);
    std::cout << "Found " << list0.size() << " files to read.\n";
    // Check for empty file
    std::vector<std::string> list;
    PBar checking(list0.size());
    checking.set_description("Checking amount of data per file.");
    size_t nlines = 0;

    if (!parallel){
        for (auto fname : list0){
            ++ checking;
            size_t count_ = count(fname);
            nlines += count_;
            if (count_ > 0){
                list.push_back(fname);
            }
        }
    } else {
        // parallel version
        /* // naive async creates a too many files open error with fopen

        std::cout << "Using multi-threaded count\n";
        // set tasks
        std::vector<std::future<size_t>> results;
        for (std::string fname: list0){
            results.emplace_back(std::async(std::launch::async, count, fname));
        }
        for (size_t k=0; k < results.size(); ++k, ++ checking){
            auto& result = results[k];
            size_t count_ = result.get();
            nlines += count_;
            if (count_ > 0){
                list.push_back(list0[k]);
            }
        }
        */
        // may return 0 when not able to detect
        const size_t nworkers = std::thread::hardware_concurrency();
        std::cout << "Using multi-threaded Pool("<< nworkers << ") count\n";
        ThreadPool pool(nworkers);

        // enqueue and store future
        std::vector<std::future<size_t>> results;
        for (std::string fname: list0){
            results.emplace_back(pool.enqueue(count, fname));
        }
        for (size_t k=0; k < results.size(); ++k, ++ checking){
            auto& result = results[k];
            size_t count_ = result.get();
            nlines += count_;
            if (count_ > 0){
                list.push_back(list0[k]);
            }
        }
    }
    checking.finish();
    std::cout << "     of which " << list.size() << " are not empty and will be combined.\n";
    std::cout << "     containing a total of " << nlines << " lines.\n";

    return 0;
}