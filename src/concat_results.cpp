/**
 * Concatenates many ascii outputs into a single file.
 *
 * Assumes
 *    * every file is structurally consistent with the others. (no checks)
 *    * files only contain a 1 line for the header
 *
 * Returns the content of all files found without their headers (only the first
 * header is kept. Ordering is not imposed in the outputs.
 */
#include <string>
#include <iostream>
// #include <cstdio>
#include <fstream>
// Bob libraries
#include <pbar.hpp>
#include <bob/helpers.hpp>
#include <mfio.hpp>
#include <locale>


int main(int argc, char * argv[]){

    //std::string current_exec_name = argv[0]; // Name of the current exec program

    std::string input_path = "results";
    std::string input_pattern = ".*.csv";
    std::string outputfilename = "catalog.csv";

    if (argc > 3) {
        input_path = argv[1];
        input_pattern = argv[2];
        outputfilename = argv[3];
    } else {
        std::cout << "Wrong number of parameters or argmuments\n"
                  << "USAGE: " << argv[0]
                  << " <directory> <pattern> <outputfile>\n"
                  << "\n"
                  << "Example: " << argv[0] << " results '.*.csv' catalog.csv\n"
                  << "\n";
        return 1;
    }

    // set cout using clean integers
    std::cout.imbue(std::locale(""));
    std::vector<std::string> list0 = list_dir(input_path, input_pattern);
    std::cout << "Found " << list0.size() << " files to combine.\n";
    // Check for empty file
    std::vector<std::string> list;
    PBar checking(list0.size());
    checking.set_description("Checking amount of data per file.");
    std::copy_if (list0.begin(), list0.end(),
                  std::back_inserter(list),
                  [&checking](std::string fname){
                      ++checking;
                      return io::count_lines(fname, "#") > 0;
                      } );
    std::cout << "     of which " << list.size() << " are not empty and will be combined.\n";


    std::ofstream output(outputfilename);

    std::ifstream indata;
    std::string line;
    size_t ndata = 0;
    PBar pb(list.size());
    for(size_t i=0; i < list.size(); ++i, ++pb){
        indata.open(list[i]);
        // First line
        std::getline(indata, line);
        if (i == 0){
            // copy header
            output << line << "\n";
        }
        while (std::getline(indata, line)){
            output << line << "\n";
            ndata ++;
        }
        indata.close();
    }
    pb.finish();
    std::cout << "Combined " << ndata << " entries.\n";
    return 0;
}
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
