/**
 * @file join.cpp
 * @author Morgan Fouesneau
 * @brief  Contains the necessary code to join (left, right or inner) two datafiles (works with vaex hdf5)
 *         The output will be in CSV.
 * @version 0.1
 * @date 2020-10-30
 *
 */
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <fstream>
#include <mfio.hpp>
#include <pbar.hpp>
#include <mfopts.hpp>
#include <bob/vaextable.hpp>


namespace str {
    /**
     * Split a string into a vector given a delimiter.
     *
     * note that consecutive delimiters are processed as one.
     *
     * @param line string to split
     * @param delimiter delimiter of the tokens
     *
     * @return vector of tokens
     */
    std::vector<std::string> split(const std::string& line, const char delimiter){

        std::vector<std::string> tokens;
        std::string token;

        std::for_each(line.begin(), line.end(), [&](char c){
            if (c != delimiter){
                token += c;
            } else {
                tokens.push_back(token);
                token.clear();
            }
            });
        if (token.length()) tokens.push_back(token);

        return tokens;
    }

    /**
     * join a vector of strings
     *
     * @param vec     values to join
     * @param delim   delimiter to use
     * @return joined string
     */
    std::string join(const std::vector<std::string>& vec, const char* delim)
    {
        std::stringstream res;
        copy(vec.begin(), vec.end(), std::ostream_iterator<std::string>(res, delim));
        return res.str();
    }

    /**
     * join a vector of strings
     *
     * @param vec     values to join
     * @param delim   delimiter to use
     * @return joined string
     */
    std::string join(const std::vector<std::string>& vec, const std::string& delim){
        return join(vec, delim.c_str());
    }
}; // end namespace str


namespace join {

    using tab_t = std::vector<std::vector<std::string>>;

    struct table {
        join::tab_t data;
        std::vector<std::string> header;
        std::string index_name;
        size_t index;
    };


    /**
     * @brief Get the joined header object of two tables
     *
     * @param left_tab    left table
     * @param right_tab   right table
     * @param l_suffix    suffix to add on the left if field exists in both tables
     * @param r_suffix    suffix to add on the right if field existis in both tables
     * @return header of the joined table
     */
    std::vector<std::string> get_joined_header(const join::table& left_tab,
                                                const join::table& right_tab,
                                                const std::string& l_suffix = "",
                                                const std::string& r_suffix = "_1"){
        std::set<std::string> right_header_set;
        for (auto value: right_tab.header){right_header_set.insert(value);}
        std::set<std::string> left_header_set;
        for (auto value: left_tab.header){left_header_set.insert(value);}

        std::vector<std::string> header;
        for (auto value: left_tab.header){
            if (right_header_set.find(value) != right_header_set.end()){
                header.push_back(value + l_suffix);
            } else {
                header.push_back(value);
            }
        }
        for (auto value: right_tab.header){
            if (left_header_set.find(value) != left_header_set.end()){
                header.push_back(value + r_suffix);
            } else {
                header.push_back(value);
            }
        }
        return header;
    }

    /**
     * Export joined table to csv
     *
     * @param output_file_name   where to export
     * @param tab                which table
     * @param delimiter          delimiter (default ',')
     * @param comment            comment character (default '#')
     */
    void export_tab_to_csv(const std::string& output_file_name, const join::table& tab,
                        const std::string delimiter=",", const std::string comment="#"){
        std::cout << "Exporting joined table into " + output_file_name + "... " << std::flush;
        io::AsciiWriter wt(output_file_name);
        wt.set_delimiter(delimiter);
        wt.set_comment(comment);
        wt << tab.header;
        ++wt;
        PBar pb(tab.data.size());
        pb.set_description("Exporting into " + output_file_name);
        for (const auto& line: tab.data){
            wt << line;
            ++wt;
            ++pb;
        }
        pb.finish();
    }


    /**
     * Inner join
     *
     * Keep only entries in both tables
     *
     * @param a         left table
     * @param columna   index of the column to use on the left
     * @param b         left table
     * @param columnb   index of the column to use on the right
     *
     * @return joined table
     *
     */
    tab_t inner(const tab_t& a, size_t columna, const tab_t& b, size_t columnb) {
        std::unordered_multimap<std::string, size_t> hashmap;
        // Index
        PBar pb(a.size());
        pb.set_description("indexing");
        for(size_t i = 0; i < a.size(); ++i) {
            hashmap.insert(std::make_pair(a[i][columna], i));
        }
        pb.finish();
        // map
        tab_t result;
        pb.reset(b.size());
        pb.set_description("inner join");
        for(size_t i = 0; i < b.size(); ++i, ++pb) {
            auto range = hashmap.equal_range(b[i][columnb]);
            for(auto it = range.first; it != range.second; ++it) {
                tab_t::value_type row;
                row.insert(row.end() , a[it->second].begin() , a[it->second].end());
                row.insert(row.end() , b[i].begin()          , b[i].end());
                result.push_back(std::move(row));
            }
        }
        pb.finish();
        return result;
    }

    /**
     * right outer join.
     *
     * Keep all entries on the right
     *
     * @param a         left table
     * @param columna   index of the column to use on the left
     * @param b         left table
     * @param columnb   index of the column to use on the right
     *
     * @return joined table
     *
     */
    tab_t right(const tab_t& a, size_t columna, const tab_t& b, size_t columnb) {
        std::unordered_multimap<std::string, size_t> hashmap;
        // hash
        PBar pb(a.size());
        pb.set_description("indexing");
        for(size_t i = 0; i < a.size(); ++i) {
            hashmap.insert(std::make_pair(a[i][columna], i));
        }
        pb.finish();

        // map
        size_t n_col_a = a[0].size();
        tab_t result;
        pb.reset(b.size());
        pb.set_description("right join");
        for(size_t i = 0; i < b.size(); ++i, ++pb) {
            auto range = hashmap.equal_range(b[i][columnb]);
            bool empty = range.first == range.second;
            tab_t::value_type empty_a(n_col_a);
            if (empty){
                tab_t::value_type row;
                row.insert(row.end() , empty_a.begin() , empty_a.end());
                row.insert(row.end() , b[i].begin()          , b[i].end());
                result.push_back(std::move(row));
            } else {
                for(auto it = range.first; it != range.second; ++it) {
                    tab_t::value_type row;
                    row.insert(row.end() , a[it->second].begin() , a[it->second].end());
                    row.insert(row.end() , b[i].begin()          , b[i].end());
                    result.push_back(std::move(row));
                }
            }
        }
        pb.finish();
        return result;
    }

    /**
     * left outer join.
     *
     * Keep all entries on the left
     *
     * @param a         left table
     * @param columna   index of the column to use on the left
     * @param b         left table
     * @param columnb   index of the column to use on the right
     *
     * @return joined table
     *
     */
    tab_t left(const tab_t& b, size_t columnb, const tab_t& a, size_t columna) {
        std::unordered_multimap<std::string, size_t> hashmap;
        // hash
        PBar pb(a.size());
        pb.set_description("indexing");
        for(size_t i = 0; i < a.size(); ++i) {
            hashmap.insert(std::make_pair(a[i][columna], i));
        }
        pb.finish();

        // map
        size_t n_col_a = a[0].size();
        tab_t result;
        pb.reset(b.size());
        pb.set_description("left join");
        for(size_t i = 0; i < b.size(); ++i, ++pb) {
            auto range = hashmap.equal_range(b[i][columnb]);
            bool empty = range.first == range.second;
            tab_t::value_type empty_a(n_col_a);
            if (empty){
                tab_t::value_type row;
                row.insert(row.end(), b[i].begin(), b[i].end());
                row.insert(row.end(), empty_a.begin(), empty_a.end());
                result.push_back(std::move(row));
            } else {
                for(auto it = range.first; it != range.second; ++it) {
                    tab_t::value_type row;
                    row.insert(row.end(), b[i].begin(), b[i].end());
                    row.insert(row.end(), a[it->second].begin(), a[it->second].end());
                    result.push_back(std::move(row));
                }
            }
        }
        pb.finish();
        return result;
    }

} // end namespace join


/**
 * Nice display of tabular data
 *
 * @param o   output stream
 * @param t   table data
 * @return output stream
 */
std::ostream& operator<<(std::ostream& o, const join::tab_t& t) {
    for(size_t i = 0; i < t.size(); ++i) {
        o << i << ":";
        for(const auto& e : t[i])
        o << ',' << e;
        o << std::endl;
    }
    return o;
}


/**
 * Get the csv table object from ascii input
 *
 * @param input_path  input data path
 * @param index_name  name of the index column
 * @param comment     comment character
 * @param delimiter   field delimiter
 * @return data from the ascii file.
 */
join::table get_csv_table(const std::string input_path,
                          const std::string index_name,
                          const std::string comment="#",
                          const char delimiter=',')
{
    std::ifstream stream(input_path);

    std::string line;
    std::string prevline;

    join::tab_t data;
    join::tab_t header;

    line.clear();
    while(std::getline(stream, line)){
        if (comment.find_first_of(line) == 0){ continue; }
        header.push_back(str::split(line, delimiter));
        break;
    }
    size_t n = io::count_lines(input_path, comment) - 1;
    PBar pb(n);
    pb.set_description("reading " + input_path);
    while(std::getline(stream, line)){
        if (comment.find_first_of(line) == 0){ continue; }
        data.push_back(str::split(line, delimiter));
        ++pb;
    }
    pb.finish();
    size_t index = 0;
    bool found = false;
    for (const std::string val: header[0]){
        if (index_name.compare(val) == 0){
            found = true;
            break;
        }
        index ++;
    }
    if (!found){ throw std::runtime_error("key " + index_name + " not found.");}

    join::table csv_table;
    csv_table.header = header[0];
    csv_table.data = data;
    csv_table.index_name = index_name;
    csv_table.index = index;
    return csv_table;
}

namespace vaex{

    /**
     * @brief Convert a vector to string data
     *
     * @tparam T    type of the data
     * @param data  data vector
     * @return vector of the data converted to strings
     */
    template <typename T>
    std::vector<std::string> to_string(const std::vector<T>& data){
        std::vector<std::string> values(data.size());
        for(size_t k=0; k <= data.size(); ++k){
            values[k] = std::to_string(data[k]);
        }
        return values;
    }

    /**
     * @brief Convert Vaex hdf5 data to string depending on the declared type
     *
     * @param ds    Vaex dataset
     * @param key   field of the data
     * @return      ds[key] as a vector of strings
     */
    std::vector<std::string> get(VaexTable* ds, std::string key){
        std::string type = ds->types.at(key);
        if (type.compare("Float64") == 0){
            // return std::to_string(ds->get_data<double>(key, from, to));
            return to_string<double>(ds->get_data<std::vector<double>>(key));
        } else if ((type.compare("Float32") == 0) ||
            (type.compare("Float16") == 0)){
                return to_string<float>(ds->get_data<std::vector<float>>(key));
        } else if (type.compare("Integer64") == 0) {
                return to_string(ds->get_data<std::vector<size_t>>(key));
        } else if (type.compare("Integer32") == 0) {
                return to_string(ds->get_data<std::vector<long>>(key));
        } else if (type.compare("Integer16") == 0) {
                return to_string<int>(ds->get_data<std::vector<int>>(key));
        }
        return ds->get_data<std::vector<std::string>>(key);
    }

    /**
     * Get the vaex table object from input file
     *
     * @param path        path to the input hdf5 file (vaex format)
     * @param index_name  column to use as index
     * @return join::table with the appropriate format.
     */
    join::table get_vaex_table(std::string path, std::string index_name){

        VaexTable ds(path);

        join::tab_t data_t;
        std::vector<std::string> header;
        size_t index = 0;
        bool found = false;
        PBar pb(ds.keys.size());
        pb.set_description("reading hdf5 data");
        for (auto key: ds.keys){
            header.push_back(key);
            data_t.push_back(get(&ds, key));
            if (key.compare(index_name) == 0){
                found = true;
            }
            if (!found){
                index ++;
            }
            ++pb;
        }
        pb.finish();

        join::tab_t data;
        size_t n_fields = data_t.size();
        size_t n_data = data_t[0].size();
        for (size_t k=0; k < data_t[0].size(); ++k){

            std::vector<std::string> row (data_t.size());
            for (size_t field=0; field < n_fields; ++field){
                row[field] = data_t[field][k];
            }
            data.push_back(std::move(row));
        }
        join::table tab;
        tab.data = data;
        tab.header = header;
        tab.index = index;
        tab.index_name = index_name;
        return tab;
    }
}; // namespace vaex


/**
 * Nice representation of table objects with cout
 */
std::ostream& operator<<(std::ostream& o, const join::table& t) {
    o << "table content\n"
      << "     Number of fields: " << t.header.size() << "\n"
      << "    Number of entries: " << t.data.size() << "\n";
    if (t.index_name.size() > 0){
      o << "                Index: " << t.index_name << "(" << t.index << ")\n";
    }
    return o;
}

/**
 * Nice representation of vector objects with cout
 */
template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> vect) {

    size_t n = vect.size();
    for (size_t i=0; i < n; ++i){
        std::cout << vect[i];
        if (i != n-1){
            std::cout << ", ";
        }
    }
    return os << std::endl;
}


int main(int argc, char *argv[])
{
    mfopts::Options opt(argv[0]);
    opt.add_option("-h,--help",     "display help message");
    opt.add_option("--lsuffix",     "left suffix in case of identical field names (default none)");
    opt.add_option("--rsuffix",     "right suffix in case of identical field names (default: _1)");
    opt.add_option("-o,--on",       "index on both left and right tables (default: source_id)");
    opt.add_option("-l,--left_on",  "index on the left table");
    opt.add_option("-r,--right_on", "index on the right table");
    opt.add_option("-j,--join",     "Join type (inner, left, right)");
    opt.add_option("-O,--output",   "output table filename");
    opt.add_option("--left_vaex",   "set if the left table is in vaex format");
    opt.add_option("--right_vaex",  "set if the right table is in vaex format");

    opt.parse_options(argc, argv);

    if (opt.has_option("-h") || (argc < 2)){
        std::cout<< opt.help();
        exit(0);
    }
    // set cout using clean integers
    std::cout.imbue(std::locale(""));

    std::string left_on = "source_id";
    std::string right_on = "source_id";

    std::string comment = "#";
    std::string delimiter = ",";
    std::string l_suffix = "";
    std::string r_suffix = "_1";
    std::string output_file_name = "joined_catalog.csv";

    std::string join_type = "inner";

    if (opt.has_option("--lsuffix")){
        l_suffix = opt.get_option<std::string>("--lsuffix");
    }
    if (opt.has_option("--rsuffix")){
        r_suffix = opt.get_option<std::string>("--rsuffix");
    }
    if (opt.has_option("--rsuffix")){
        r_suffix = opt.get_option<std::string>("--rsuffix");
    }
    if (opt.has_option("--join")){
        join_type = opt.get_option<std::string>("--join");
    }
    if (opt.has_option("--on")){
        left_on = opt.get_option<std::string>("--on");
        right_on = opt.get_option<std::string>("--on");
    }
    if (opt.has_option("--left_on")){
        left_on = opt.get_option<std::string>("--left_on");
    }
    if (opt.has_option("--right_on")){
        right_on = opt.get_option<std::string>("--right_on");
    }
    if (opt.has_option("--output")){
        output_file_name = opt.get_option<std::string>("--output");
    }

    std::string left_path = argv[1];
    std::string right_path = argv[2];

    std::cout << "\n"
              << " Joining catalogs \n"
              << "==================\n"
              << left_path << " "
              << join_type << " join "
              << right_path
              << " on (" << left_on << ", " << right_on << ")"
              << "\n"
              << "     l_suffix: " << l_suffix
              << "     r_suffix: " << r_suffix
              << "       output: " << output_file_name
              << "\n\n";


    std::cout << "Loading left table... " << std::flush;
    join::table left_tab;
    if (opt.has_option("--left_vaex")){
        left_tab = vaex::get_vaex_table(left_path, left_on);
        // join::export_tab_to_csv(left_path + ".csv", left_tab);
    } else {
        left_tab = get_csv_table(left_path, left_on);
    }

    std::cout << "\nLeft Table: " << left_path << "\n"
              << left_tab
              << "\n";

    std::cout << "Loading right table... " << std::flush;
    join::table right_tab;
    if (opt.has_option("--right_vaex")){
        right_tab = vaex::get_vaex_table(right_path, left_on);
    } else {
        right_tab = get_csv_table(right_path, right_on);
    }

    std::cout << "\nRight Table: " << right_path << "\n"
              << right_tab
              << "\n";

    std::cout << "Joining tables... " << std::flush;
    join::tab_t joined_data;
    if (join_type.compare("inner") == 0){
        joined_data = join::inner(left_tab.data, left_tab.index, right_tab.data, right_tab.index);
    } else if (join_type.compare("left") == 0){
        joined_data = join::left(left_tab.data, left_tab.index, right_tab.data, right_tab.index);
    } else if (join_type.compare("right") == 0){
        joined_data = join::right(left_tab.data, left_tab.index, right_tab.data, right_tab.index);
    }

    auto header = join::get_joined_header(left_tab, right_tab);
    join::table joined;
    joined.data = joined_data;
    joined.header = header;

    join::export_tab_to_csv(output_file_name, joined);

    std::cout << "\nJoined Table: " << output_file_name << "\n"
              << joined
              << "\n";

    std::cout << "done.\n";
    return 0;
}