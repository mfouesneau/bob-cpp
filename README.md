# cpp-bob

BOB in c++ -- An astrophysical parameter determination with model emulation and MCMC

[![pipeline status](https://gitlab.com/mfouesneau/bob-cpp/badges/master/pipeline.svg)](https://gitlab.com/mfouesneau/bob-cpp/-/commits/master)
[![coverage report](https://gitlab.com/mfouesneau/bob-cpp/badges/master/coverage.svg)](https://gitlab.com/mfouesneau/bob-cpp/-/commits/master)
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/mfouesneau/bob-cpp)
