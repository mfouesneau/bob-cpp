import sys
import numpy as np 
import pylab as plt
import pandas as pd
from corner import corner
from ezdata.matplotlib import light_minimal
plt.style.use(light_minimal)


prefix = sys.argv[1]
sampling_file = prefix + '_samples.csv'
input_file = prefix + '_input.csv'


mapping = (
    ("name"              , "source_id" , "-")              ,
    ("parallax"          , "parallax"  , "parallax_error") ,
    ("Gaia_MAW_BP_faint" , "bpmag"     , "bpmag_error")    ,
    ("Gaia_MAW_G"        , "gmag"      , "gmag_error")     ,
    ("Gaia_MAW_RP"       , "rpmag"     , "rpmag_error")    ,
    ("2MASS_J"           , "j_m"       , "j_msigcom")      ,
    ("2MASS_H"           , "h_m"       , "h_msigcom")      ,
    ("2MASS_Ks"          , "ks_m"      , "ks_msigcom")     ,
    ("WISE_RSR_W1"       , "w1mpro"    , "w1mpro_error")   ,
    ("WISE_RSR_W2"       , "w2mpro"    , "w2mpro_error"))


df = pd.read_csv(sampling_file).drop('chain', axis=1)
dfi = pd.read_csv(input_file)

df = df[np.isfinite(df.lnp)]


sed_names = [k[0] for k in mapping[2:]]
mcmc_trace = df[[col for col in df.columns if col not in sed_names if df[col].std() > 1e-8]]
sed_trace = df[[col for col in df.columns if col in sed_names]]
sed_obs = dfi[[col for col in df.columns if col in sed_names]]
sed_err = dfi[[col + '_unc' for col in df.columns if col in sed_names]]


K = len(mcmc_trace.columns)
fig, _ = plt.subplots(K, K, figsize=(20, 20))
try:
    corner(mcmc_trace.to_numpy(),
           labels=mcmc_trace.columns,
           quantiles=(0.16, 0.50, 0.84), show_titles=True,
           title_kwargs={'fontsize': 'small'},
           fig=fig)
except Exception as ex:
    print(ex)


def clean_sed_names(names):
    def cleaning(name):
        split_name = name.split('_')
        if 'faint' in name.lower():
            return split_name[-2]
        return split_name[-1]
    return [cleaning(name) for name in names]


ypred = sed_trace.to_numpy()
ynames = clean_sed_names(sed_trace.columns)
ax = plt.axes([0.65, 0.65, 0.30, 0.30])
percs = np.percentile(ypred, np.arange(0, 101, 10), axis=0)
plt.plot(percs.T, lw=0.2, color='k')
percs = np.percentile(ypred, [16, 50, 84], axis=0)
plt.plot(percs.T, lw=0.7, color='k')
ax.set_title('source_id = {0:d}'.format(dfi.name[0]), fontsize='small')
plt.xticks(np.arange(len(ynames)), ynames, rotation=90)
plt.ylim(plt.ylim()[::-1])

y = np.squeeze(sed_obs.to_numpy())
yerr = np.squeeze(sed_err.to_numpy())
plt.errorbar(np.arange(len(y)), y, yerr=yerr,
             linestyle='None', capsize=0, color='r', marker='o')

plt.savefig(prefix + '.png', bbox_inches='tight')
