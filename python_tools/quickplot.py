import pylab as plt
import numpy as np
from matplotlib import rcParams
from ezdata import SimpleTable
from ezdata.datashader import DSPlotter
from ezdata.matplotlib import light_minimal
from ezdata.matplotlib import generate_cmap_from_colors
from ezdata.dask import read_vaex_table
plt.style.use(light_minimal)


mpl_colors = rcParams['axes.prop_cycle'].by_key()['color']

spread = 2
p_color = mpl_colors[1]


fname = 'last_concat.csv'
catalog = 'data/orionsample.vaex.hdf5'
df = SimpleTable(fname)
for k in df.keys():
    if k != '':
        if k[0] == '2':
            df.add_column('TM_' + '_'.join(k.split('_')[1:]), df[k])
print(f"Reading from {fname:s} and {catalog:s}")
print("... {0:,d} processed sources.".format(len(df)))

plt.figure(figsize=(12, 10))
lw = 0.02 / np.log10(len(df))

plt.subplot(221)
ds = read_vaex_table(catalog)
joined = ds.set_index('source_id').join(
    df.to_dask(npartitions=1)
      .set_index('source_id'), lsuffix='_in', how='left').compute()

im = DSPlotter(joined).select([None, 'logT_p50 == logT_p50'])\
                      .plot('l', 'b', alpha_below=1, spread=spread)
plt.colorbar(im[0]).set_label('count')
plt.xlabel(r'$\ell$ [deg]')
plt.ylabel(r'b [deg]')

plt.subplot(222)
im = DSPlotter(df.to_pandas()).plot(
    '1e3/parallax',
    '10 ** (dmod_p50 / 5 + 1)', alpha_below=1,
    cmap=generate_cmap_from_colors([p_color, 'w']), spread=spread)
plt.colorbar(im).set_label('count')
plt.xlabel(r'$10^3$mas / parallax [pc]')
plt.ylabel(r'$10^{\mu_{p50} / 5 + 1}$ [pc]')

plt.xlim(0, 1.2e3)
plt.ylim(0, 1.2e3)
plt.plot(plt.xlim(), plt.xlim(), color='0.5', lw=2, zorder=-100)

plt.errorbar(
    df('1e3/parallax'),
    df('10 ** (dmod_p50 / 5 + 1)'),
    # xerr=(df('1e3/(parallax + parallax_sigma)'),
    #       df('1e3/(parallax - parallax_sigma)')),
    yerr=(df('10 ** (dmod_p50 / 5 + 1)') - df('10 ** (dmod_p16 / 5 + 1)'),
          df('10 ** (dmod_p84 / 5 + 1)') - df('10 ** (dmod_p50 / 5 + 1)')),
    linestyle='None', lw=lw, color='k', rasterized=True,
    zorder=-100)

plt.vlines([400, 600], 0, plt.ylim()[1], color='k',
           linestyle=':', lw=2)
plt.text(500, 100, "Orion", va='center', ha='center', color='k')


plt.subplot(223)

im = DSPlotter(df.to_pandas()).plot(
    'logT_p50', 'logL_p50',
    alpha_below=1,
    cmap=generate_cmap_from_colors([p_color, 'w']), spread=spread)
plt.colorbar(im).set_label('count')
plt.xlabel(r'log$_{10}$(T$_{eff}$/K)')
plt.ylabel(r'log$_{10}$(L/L$_\odot$)')
plt.xlim(4.7, 3.2)
plt.ylim(-3.5, 6)

plt.errorbar(
    df('logT_p50'),
    df('logL_p50'),
    xerr=(df('logT_p50-logT_p16'), df('logL_p84-logL_p50')),
    yerr=(df('logL_p50-logL_p16'), df('logL_p84-logL_p50')),
    linestyle='None', lw=lw, color='k', rasterized=True,
    zorder=-100)

plt.subplot(224)
im = DSPlotter(df.to_pandas()).plot(
    'TM_J -TM_Ks', 'TM_Ks + 5 * log10(parallax/100)',
    alpha_below=1, 
    cmap=generate_cmap_from_colors([p_color, 'w']), spread=spread)
plt.colorbar(im).set_label('count')
plt.xlabel(r'J-K$_S$ [mag]')
plt.ylabel(r'K$_S$ + 5 log$_{10}$ ($\varpi$/100) [mag]')
plt.ylim(plt.ylim()[::-1])

plt.tight_layout()

plt.savefig('diagnostic_parallax_distance.png')

joined.to_csv('joined_catalog.csv', index=False)

plt.show()
