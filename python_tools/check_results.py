import os
from ezdata.matplotlib.light_minimal_theme import light_minimal
from ezdata.datashader import DSPlotter, generate_cmap_from_colors
import dask.dataframe as dd
import pylab as plt
import matplotlib
from glob import glob
plt.style.use(light_minimal)
colors = matplotlib.rcParams['axes.prop_cycle'].by_key()['color']


def is_non_zero_file(fpath, minsize=0):
    """ Check if a file is non zero size """
    if os.path.isfile(fpath) and (os.path.getsize(fpath) > minsize):
        return True
    return False


pattern = 'results/*csv'
print('... looking for {0:s}'.format(pattern))
print('... found {0:d} files'.format(len(glob(pattern))))
lst = sorted([k for k in glob(pattern) if is_non_zero_file(k, 3000)])
print('... of which {0:d} are non-empty'.format(len(lst)))
ds = dd.read_csv(lst).set_index('source_id')
ds.to_csv('last_concat.csv', single_file=True)
# df = ds.compute()

print("Results contain {0:,d} entries".format(len(ds)))

spread = 5
plt.figure()
im = DSPlotter(ds).select(['parallax > 0'])\
                  .plot("log10(1e3/parallax)", "(dmod_p50 / 5. + 1)",
                        spread=spread,
                        alpha_below=1, cmap="Blues_r",
                        norm='log10')[0]
plt.colorbar(im).set_label('count')
plt.xlabel('log(1e3/parallax)')
plt.ylabel('log(median distance)')
plt.tight_layout()
plt.savefig('parallax_distance.png')

"""
plt.figure()
im = DSPlotter(ds).select(['parallax > 0'])\
                  .plot("log10(parallax)", "(2. - dmod_p50 / 5.)",
                        spread=spread,
                        alpha_below=1, cmap=plt.cm.Blues_r,
                        norm='log10')[0]
plt.colorbar(im).set_label('count')
plt.xlabel('log parallax')
plt.ylabel('log median parallax')
plt.tight_layout()
plt.savefig('parallax_distance.png')
"""

plt.figure()
im = DSPlotter(ds).select([None])\
                  .plot("logT_p50", 'logL_p50', spread=spread,
                        alpha_below=1,
                        cmap=generate_cmap_from_colors([colors[0], 'w']),
                        norm='log10')
im = DSPlotter(ds).select(['lnp_p50 > -100'])\
                  .plot("logT_p50", 'logL_p50', spread=spread,
                        alpha_below=1,
                        cmap=generate_cmap_from_colors([colors[1], 'w']),
                        norm='log10')
plt.colorbar(im[0]).set_label('count')
plt.xlim(4.01, 3.45)
plt.ylim(-2.05, 4.05)
plt.xlabel('log(T/K)')
plt.ylabel('log(L/Lsun)')
plt.tight_layout()

plt.savefig('hrd.png')
plt.show()
