import h5py
import numpy as np


def get_x_props(hdf):
    x_max = []
    x_min = []
    x_mean = []
    for k in hdf['/models'].keys():
        model = hdf['/models/{0}'.format(k)]
        x_min.append(model['x_min'][()])
        x_max.append(model['x_max'][()])
        x_mean.append(model['x_mean'][()])

    x_min = np.array(x_min)
    x_max = np.array(x_max)
    x_mean = np.array(x_mean)

    return x_mean, x_min, x_max


def get_y_props(hdf):
    y_max = []
    y_min = []
    y_mean = []
    for k in hdf['/models'].keys():
        model = hdf['/models/{0}'.format(k)]
        y_min.append(model['y_min'][()])
        y_max.append(model['y_max'][()])
        y_mean.append(model['y_mean'][()])

    y_min = np.array(y_min)
    y_max = np.array(y_max)
    y_mean = np.array(y_mean)

    return y_mean, y_min, y_max


def get_star():
    """
    Star object 18616381125975296
    --------------------------------
    (recalibrated)
             parallax: 2.84578 (+/- 0.0709308)
    Gaia_MAW_BP_faint: 13.0993 (+/- 0.06)
           Gaia_MAW_G: 12.7048 (+/- 0.06)
          Gaia_MAW_RP: 12.1969 (+/- 0.06)
              2MASS_J: 11.559 (+/- 0.023)
              2MASS_H: 11.259 (+/- 0.023)
             2MASS_Ks: 11.166 (+/- 0.021)
          WISE_RSR_W1: 11.137 (+/- 0.022)
          WISE_RSR_W2: 11.174 (+/- 0.021)
    """
    return {"parallax": 2.84578,
            "Gaia_MAW_BP_faint": 13.0993,
            "Gaia_MAW_G": 12.7048,
            "Gaia_MAW_RP": 12.1969,
            "2MASS_J": 11.559,
            "2MASS_H": 11.259,
            "2MASS_Ks": 11.166,
            "WISE_RSR_W1": 11.137,
            "WISE_RSR_W2": 11.174}


f = h5py.File('emulators/oldgrid_with_r0_w_adjacent.emulator.hdf5', 'r')
keys = np.array([k for k in f['/models']])
X = get_x_props(f)
Y = get_x_props(f)

star = get_star()
dmod_guess = 5 * np.log10(1e2 / star['parallax'])

if (dmod_guess > 18):
    dmod_guess = 18.0
