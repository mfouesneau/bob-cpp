"""  Download the data from the archive on the fly
"""
from astroquery.gaia import Gaia
from astroquery.exceptions import TimeoutError

QUERY_TEMPLATE = """
SELECT
    random_index,
    gaia.source_id, gaia.ra, gaia.dec, gaia.l, gaia.b,
    gaia.parallax, gaia.parallax_error,
    gaia.phot_g_mean_mag as Gmag, 1./phot_g_mean_flux_over_error as Gmag_error,
    gaia.phot_bp_mean_mag as BPmag, 1./phot_bp_mean_flux_over_error as BPmag_error,
    gaia.phot_rp_mean_mag as RPmag, 1./phot_rp_mean_flux_over_error as RPmag_error,
    tmass.j_m, tmass.j_msigcom,
    tmass.h_m, tmass.h_msigcom,
    tmass.ks_m, tmass.ks_msigcom,
    wise.w1mpro, wise.w1mpro_error,
    wise.w2mpro, wise.w2mpro_error
FROM gaiadr2.gaia_source as gaia
INNER JOIN gaiadr2.tmass_best_neighbour as tmass_x
ON tmass_x.source_id = gaia.source_id
INNER JOIN gaiadr1.tmass_original_valid as tmass
ON tmass.tmass_oid = tmass_x.tmass_oid
INNER JOIN gaiadr2.allwise_best_neighbour as wise_x
ON wise_x.source_id = gaia.source_id
INNER JOIN gaiadr1.allwise_original_valid as wise
ON wise.allwise_oid = wise_x.allwise_oid
WHERE random_index between {random_index_min:d} and {random_index_max:d}
AND parallax BETWEEN {parallax_min:f} AND {parallax_max:f}
"""

defaults = {}
defaults = {'parallax_max': 1000.,   # more than 1pc
            'parallax_min':  0.02,   # less than 50 kpc
            }
# Note: this query returns about 2% of the data on the first chunk
#               1_000_000 --> 21_697    (4 MB)
#                 500_000 --> 10_847    (2 MB)
#                 100_000 -->  2_213    (500 KB)


def check_running_queue(nmax=20):
    """ Return if the current queue is running max jobs already """
    total = sum('EXECUTING' in job.get_phase()
                for job in Gaia.list_async_jobs())
    print(f"The queue is running {total:d} jobs")
    return total < nmax


def wait_for(fn):
    """ wait for an function to return True """
    import time
    default_sleep = 60   # seconds

    def wrap(*args, **kwargs):
        result = fn(*args, **kwargs)
        while (not result):
            time.sleep(default_sleep)
            result = fn(*args, **kwargs)
        return True
    return wrap


def get_default_data(index_min, index_max):
    config = dict(output_directory='data', project_name='lbol-gdr2',
                  random_index_min=index_min, random_index_max=index_max)
    config.update(**defaults)
    output_file = r'{output_directory:s}/{project_name:s}_{random_index_min:d}_{random_index_max:d}.csv'
    print("Getting: ", index_min, index_max)
    try:
        open(output_file.format(**config), 'r').close()
        print(u'\N{check mark} ' + "File already downloaded.")
    except Exception as e:
        print(e)
        print("Downloading the data ...")
        print(QUERY_TEMPLATE.format(**config))
        Gaia.login(credentials_file='credentials_gaia')
        print("... Waiting for a spot in the Archive queue.")
        wait_for(check_running_queue)()
        print("... Spot in the Archive queue found.")
        print("... Running query.")
        job = Gaia.launch_job_async(QUERY_TEMPLATE.format(**config),
                                    name=output_file.format(**config),
                                    output_format='csv',
                                    autorun=True,
                                    dump_to_file=True,
                                    output_file=output_file.format(**config))
        print("... Query finished.")
        print("... Data downloaded to " + output_file.format(**config))
        print("... Cleaning finished job")
        Gaia.remove_jobs(job.jobid)
        print(u'\N{check mark} ' + "Download completed.")
    return output_file.format(**config)


def timeout_retry(fn):
    def wrapper(*args, **kwargs):
        try:
            return get_default_data(*args, **kwargs)
        except Exception as e:
            print(e)
    return wrapper


def download_all():
    from joblib import Parallel, delayed
    slice_l = 10000
    slice_max = 1692919134
    range_ = range(0, slice_max // slice_l + 1)
    jobs = Parallel(n_jobs=40, verbose=100)\
            (delayed(timeout_retry(get_default_data))\
            (i * slice_l, (i + 1) * slice_l) for i in range_)
    return jobs


def download_slice(i):
    slice_l = 10000
    slice_max = 1692919134
    i = min(max(0, i), slice_max // slice_l + 1)
    fname = get_default_data(i * slice_l, (i + 1) * slice_l)
    print("\n\n")
    print(fname)
    return


if __name__ == "__main__":
    from joblib import Parallel, delayed
    slice_l = 10000
    slice_max = 1692919134
    range_ = range(0, slice_max // slice_l + 1)
    jobs = Parallel(n_jobs=40, verbose=100)\
           (delayed(timeout_retry(get_default_data))\
           (i * slice_l, (i + 1) * slice_l) for i in range_)
