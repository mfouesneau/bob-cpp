/**
 * @file base.hpp
 * @author Morgan Fouesneau
 * @brief  Definition of a Base Class Emulator used in the posterior
 * @version 0.1
 * @date 2020-10-10
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once
#include <string>
#include <iostream>
#include <math.h>
#include <cmath>
#include <cstdio>
#include <vector>
#include "xtensor/xarray.hpp"
#include "xtensor/xio.hpp"
#include "xtensor/xview.hpp"
#include "xtensor/xadapt.hpp"


typedef double DType;
// linalg package doesn't support dynamic layouts
using Matrix = xt::xarray<DType, xt::layout_type::row_major>;
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;

/**
 * Nice representation of vector objects with cout
 */
template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> vect) {

    size_t n = vect.size();
    for (size_t i=0; i < n; ++i){
        std::cout << vect[i];
        if (i != n-1){
            std::cout << ", ";
        }
    }
    return os << std::endl;
}

namespace Emulator {
    /**
     * @brief Defines the Generic API of emulators
     */
    class Base{
        public:
            std::string name = "Emulator";
            xt::xarray<std::string> feature_columns;
            xt::xarray<std::string> label_columns;

            Base(){};
            Base(const std::string& name,
                 const xt::xarray<std::string>& feature_columns,
                 const xt::xarray<std::string>& label_columns):
                 name{name}, feature_columns{feature_columns}, label_columns{label_columns}{};

            xt::xarray<std::string> names(){return this->feature_columns;};
            xt::xarray<std::string> prediction_names(){return this->label_columns;};
            shape_type shape() {return shape_type({this->feature_columns.shape(0), this->label_columns.shape(0)});};

            // virtual necessary for the derived classes
            virtual DMatrix predict(DMatrix& Xin) {return {};};
            virtual std::vector<double> predict(std::vector<double>& x) { return {};};
    };

    std::ostream &operator<<(std::ostream &os, const Base* emulator) {

        os << std::endl
        << "Mars Emulator object\n"
        << "--------------------------------" << std::endl
        << emulator->feature_columns.shape(0) << " Features: "
        << std::endl
        << std::left
        << emulator->feature_columns << std::endl
        << emulator->label_columns.shape(0) << " Labels: "
        << std::endl
        << emulator->label_columns << std::endl
        << std::endl;
        return os << std::endl;
    }

    std::ostream &operator<<(std::ostream &os, const Base& emulator) {

        os << std::endl
        << "Emulator object\n"
        << "--------------------------------" << std::endl
        << "name: " << emulator.name << std::endl
        << emulator.feature_columns.shape(0) << " Features: "
        << std::endl
        << std::left
        << emulator.feature_columns << std::endl
        << emulator.label_columns.shape(0) << " Labels: "
        << std::endl
        << emulator.label_columns << std::endl
        << std::endl;
        return os << std::endl;
    }
}; // namespace Emulator