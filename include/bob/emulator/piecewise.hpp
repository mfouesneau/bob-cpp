/**
 * Definition of the PieceWise Emulator class.
 *
 * This class is the initial start of emulators. It considers the model space binned
 * over the parameter space, and on each bin uses a "model" (here polynomial regression model).
 */
#ifndef Emulator_HH
#define Emulator_HH
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor-blas/xlinalg.hpp>
#include <xtensor/xmanipulation.hpp>
#include <xtensor-io/xhighfive.hpp>
#include <highfive/H5Easy.hpp>
#include <string>
#include <iostream>
#include <map>

// Bob libraries
#include <pbar.hpp>
#include <bob/helpers.hpp>
#include <bob/polynomialregressionmodel.hpp>
#include <bob/emulator/base.hpp>


typedef double DType;
// linalg package doesn't support dynamic layouts
using Matrix = xt::xarray<DType, xt::layout_type::row_major>;
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;


namespace Emulator {

    class PieceWise : public Base {
        public:
            // std::string name = "PieceWise";
            std::map<std::string, PolynomialRegressionModel> models;
            // xt::xarray<std::string> feature_columns;
            // xt::xarray<std::string> label_columns;
            DMatrix feature_dx;
            DMatrix limits;
            DMatrix deltas;

            PieceWise(){};
            PieceWise(const std::string& name,
                    const xt::xarray<std::string>& features,
                    const DMatrix& feature_dx,
                    const xt::xarray<std::string>& labels,
                    const DMatrix& feature_limits);
            PieceWise(const std::string& name,
                    const xt::xarray<std::string>& features,
                    const DMatrix& feature_dx,
                    const xt::xarray<std::string>& labels,
                    const DMatrix& feature_limits,
                    const std::map<std::string, PolynomialRegressionModel> models);

            // xt::xarray<std::string> names(){return this->feature_columns;};
            // xt::xarray<std::string> prediction_names(){return this->label_columns;};
            // shape_type shape();
            DMatrix predict(DMatrix& Xin, const bool strict_interpolation=false);

            xt::xarray<size_t> encode_keyfn(const xt::xarray<size_t>& positions,
                                            const int nbits=2);
            xt::xarray<size_t> decode_keyfn(const xt::xarray<size_t>& index_val,
                                            const size_t ndim,
                                            const size_t nbits=2);
            xt::xarray<size_t> get_indexes(const DMatrix& features);
            DMatrix bins_to_values(const DMatrix& bin_coords);
            DMatrix get_bins_center(const DMatrix& bin_coords);
    };

    /**
     * Constructor.
     */
    PieceWise::PieceWise(const std::string& name,
                    const xt::xarray<std::string>& features,
                    const DMatrix& feature_dx,
                    const xt::xarray<std::string>& labels,
                    const DMatrix& feature_limits){

        this->name = name;
        this->feature_columns = features;
        this->feature_dx = feature_dx;
        this->label_columns = labels;
        this->limits = feature_limits;
    }

    /**
     * Constructor.
     */
    PieceWise::PieceWise(const std::string& name,
                    const xt::xarray<std::string>& features,
                    const DMatrix& feature_dx,
                    const xt::xarray<std::string>& labels,
                    const DMatrix& feature_limits,
                    const std::map<std::string, PolynomialRegressionModel> models){

        this->name = name;
        this->feature_columns = features;
        this->feature_dx = feature_dx;
        this->label_columns = labels;
        this->limits = feature_limits;
        this->models = models;
    }


    /**
     * Encode the positions into a single key using nbits digits per dimensions.
     *
     * @param positions  index coordinates to convert
     * @param nbits      number of bits to encode the coordinates
     *
     * @return encoded index
     */
    xt::xarray<size_t> PieceWise::encode_keyfn(
            const xt::xarray<size_t>& positions,
            const int nbits){

        auto positions_ = xt::atleast_2d(positions);
        size_t ndim = positions_.shape(1);
        DMatrix shift = xt::ones<double>(positions_.shape()) * xt::arange<double>(ndim);
        DMatrix encoded_vector = positions_ * xt::pow(10, nbits * shift);
        return xt::cast<size_t>(xt::sum(encoded_vector, 1));
    }

    /**
     * Decode the indexed values into the ndim indexed coordinates
     */
    xt::xarray<size_t> PieceWise::decode_keyfn(
            const xt::xarray<size_t>& index_val,
            const size_t ndim,
            const size_t nbits){
        auto index = xt::transpose(xt::atleast_2d(index_val));
        double constant = std::pow(10, nbits);
        auto val1 = index / xt::pow(10, nbits * xt::arange<size_t>(ndim));
        auto val2 = index / xt::pow(10, nbits * xt::arange<size_t>(1, ndim + 1));
        return xt::cast<size_t>(val1 - val2 * constant);
    }


    /**
     * Create the index array of the features.
     *
     * @param features (n_samples, n_features) features to index
     *
     * @return (n_samples, n_features) indexes of the samples
     */
    xt::xarray<size_t> PieceWise::get_indexes(const DMatrix& features){
        auto features_2d = xt::atleast_2d(features);
        return xt::cast<size_t>(
            (features_2d - xt::view(this->limits, 0, xt::all())) / this->feature_dx
        );
    }

    /**
     * Predict using the model.
     *
     * @param Xin   (n_samples, n_features) Samples.
     *
     * @return (n_samples, n_labels) Returns predicted values.
     * */
    DMatrix PieceWise::predict(DMatrix& Xin, const bool strict_interpolation){

        if (this->models.empty()){
            throw std::runtime_error("Models need to be set first");
        }
        auto xin_ = xt::atleast_2d(Xin);
        xt::xarray<size_t> x_index;

        DMatrix predictions = xt::empty<double>({xin_.shape(0), this->label_columns.shape(0)});

        // std::cout << "DEBUG: predictions: " << predictions << "\n";

        std::string key;
        for( size_t i = 0; i < xin_.shape(0); ++i){
            auto Xi = xt::view<DMatrix>(xin_, i, xt::all());
            x_index = this->get_indexes(Xi);
            key = std::to_string(encode_keyfn(x_index)[0]);
            PolynomialRegressionModel model = this->models.at(key);
            auto predview = xt::view(predictions, i, xt::all());
            predview.fill(std::numeric_limits<double>::quiet_NaN());
            if (model.is_stable()) {
                auto pred = xt::view(model.predict(Xi), 0, xt::all());
                if (model.is_stable_prediction(pred)[0] > 0){
                    predview = pred;
                } else {
                    std::cout << "DEBUG: " << Xi << " instable prediction\n";
                }
            }
        }
        return predictions;
    }

    /**
     * Give the positions of the lowest corner of a bin associated with the data
     *
     * @param bin_coords   coordinates of the bins
     *
     * @return  array of positions
     */
    DMatrix PieceWise::bins_to_values(const DMatrix& bin_coords){
        auto bin_coords_ = xt::atleast_2d(bin_coords);
        return (bin_coords_ * xt::view(this->feature_dx, xt::newaxis(), xt::all()) +
                xt::view(xt::view(this->limits, 0, xt::all()), xt::newaxis(), xt::all()));
    }

    /**
     * Give the positions of the center of a bin associated with the data
     *
     * @param bin_coords   coordinates of the bins
     *
     * @return  array of positions
     */
    DMatrix PieceWise::get_bins_center(const DMatrix& bin_coords){
        return (this->bins_to_values(bin_coords) +
                0.5 *  xt::view(this->feature_dx, xt::newaxis(), xt::all()));
    }


    /**
     * Load an emulator from a single hdf5 file.
     *
     * CAUTION: string arrays are not working. The file needs to have
     * emulator_features_txt and emulator_labels_txt
     */
    PieceWise emulator_from_single_hdf5(
            const std::string fname = "emulator.hdf5"){

        // read the emulator configuration
        HighFive::File file(fname, HighFive::File::ReadOnly);
        std::string name = H5Easy::load<std::string>(file, "/emulator/emulator_name");
        std::string tmp;
        std::vector<std::string> tmp_split;
        tmp = H5Easy::load<std::string>(file, "/emulator/emulator_features_txt");
        tmp_split = split_string(tmp, ";");
        xt::xarray<std::string> features = xt::adapt(tmp_split, {tmp_split.size()});
        tmp = H5Easy::load<std::string>(file, "/emulator/emulator_labels_txt");
        tmp_split = split_string(tmp, ";");
        xt::xarray<std::string> labels = xt::adapt(tmp_split, {tmp_split.size()});
        DMatrix feature_dx = H5Easy::load<DMatrix>(file, "/emulator/emulator_dx");
        DMatrix feature_limits = H5Easy::load<DMatrix>(file, "/emulator/emulator_limits");
        auto models = collection_from_single_file(file);
        return PieceWise(name, features, feature_dx, labels, feature_limits, models);
    }

    /**
     * Nice representation of PieceWise objects with cout
     */
    std::ostream &operator<<(std::ostream &os, const PieceWise &emulator) {

        os << std::endl
        << "PieceWise object " + emulator.name << std::endl
        << "--------------------------------" << std::endl
        << emulator.feature_columns.shape(0) << " Features: "
        << std::endl
        << std::left
        << emulator.feature_columns << std::endl
        << emulator.label_columns.shape(0) << " Labels: "
        << std::endl
        << emulator.label_columns << std::endl
        << "Model collection contains " << emulator.models.size() << " models."
        << std::endl;
        return os << std::endl;
    }
}; // namespace Emulator

#endif // Emulator_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
