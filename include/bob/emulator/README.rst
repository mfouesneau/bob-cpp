# Emulator

In this package, I define the Emulator interface.

An emulator is a general layer that gives access to models.
In this application, it is meant to define an abstraction layer that renders SEDs in a fast manner compared to a usual interpolation scheme.