#ifndef IMF_HH
#define IMF_HH
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xmath.hpp>
#include <iostream>
#include <math.h>
#include <cmath>

typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;


namespace IMF {
	/**
	 * Stellar initial mass function distribution
	 */
	class IMF{

    	public:
		IMF(const size_t nI, const DMatrix& x, const DMatrix& a, 
		    double massMin, double massMax, const std::string& name);
		double get_value(double mass) const;

	protected:
		std::string name;
		size_t nIMFbins;
		double massMin;
		double massMax;
		DMatrix massinf;
		DMatrix slope;
		DMatrix coeffcont;
		double norm;

	};

	IMF::IMF(const size_t nI, 
		 const DMatrix& x,
		 const DMatrix& a,
		 double massMin,
		 double massMax,
		 const std::string& name):
	    nIMFbins(nI), massinf(x), slope(a), massMin(massMin), massMax(massMax),
	    name(name) {

	    this->coeffcont = xt::zeros<double>({a.shape(0)});
	    //  Make functional form
	    //  ====================
	    //  the first step is to build the functional form of the IMF:
	    //  i.e., make a continuous function and compute the normalization
	    //
	    //  continuity
	    //  ----------
	    //  given by:  c[i-1] * x[i] ^ a[i-1] = c[i] * x[i] ^ a[i]
	    //  arbitrary choice for the first point, which will be corrected by the
	    //  normalization step.
	    this->coeffcont[0] = 1.;
	    for (size_t i=1; i < nI; ++i){
		this->coeffcont[i] = (this->coeffcont[i - 1]);
		this->coeffcont[i] *= std::pow(x[i], (a[i - 1] - a[i]));
	    }
	    //  normalize
	    //  ---------
	    //  depends on the adpoted definition of the IMF indexes:
	    //  either dN/dM or dN/dlog(M). In this example we consider that indexes
	    //  are given in units of dN/dM.
	    //  get the norm : integral(IMF(M) dM) = 1 seen as a prob dist. funct."""

	    this->norm = 0.;  //  will be updated
	    DMatrix b = a + 1.;
	    // analytical integration of a power law
	    double x0, x1, S;
	    for(size_t i = 0; i < this->nIMFbins; ++i) {
		if ((massMin < x[i + 1]) && (massMax > x[i])) {
		    if (x[i] <= massMin) { 
			x0 = massMin; 
		    } else { 
			x0 = x[i]; 
		    }
		    if (x[i + 1] <= massMax){ 
			x1 = x[i + 1];
		    } else {
			x1 = massMax;
		    }

		    // careful if the index is -1
		    if (abs(b[i]) < 1e-30) {
			S = coeffcont[i] * (std::log(x1) - std::log(x0));
		    } else {
			S = coeffcont[i] / b[i] * (std::pow(x1, b[i]) - std::pow(x0, b[i]));
		    }
		    this->norm += S;
		}
	    }
	}

	/**
	 * Return the value of the normalized distribution at m
	 */
	double IMF::get_value(double m) const{
	    // extrapolation
	    if ((m > this->massMax) || (m < this->massMin))
		return 0.;
	    
	    // otherwise do the evaluation with the correct interval
	    size_t i = 0;
	    if (this->nIMFbins > 1){
		while(m > this->massinf[i]){
		    i += 1;
		}
		i -= 1;
	    }
	    if (this->massinf[i] > this->massMax){
		return 0.;
	    } else {
		return (this->coeffcont[i]) / this->norm * std::pow(m, this->slope[i]);
	    }
	}

	class Kroupa2001 : public IMF {
	    public:
		Kroupa2001() : 
		    IMF(4, 
			{0.01, 0.08, 0.5, 1., 150.},
			{-0.3, -1.3, -2.3, -2.3},
			0.01, 
			150.,
			"Kroupa2001"){}
	};
}
#endif // IMF_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
