/**
 * Reporter API to export chain statistics
 */
#pragma once
#include <xtensor/xarray.hpp>
#include <xtensor/xmath.hpp>
#include <xtensor/xsort.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor/xindex_view.hpp>
#include <xtensor/xmanipulation.hpp>
#include <string>
#include <iostream>
#include <map>
#include <set>
// #include <math.h>
#include <cmath>

// Bob libraries
#include <mfio.hpp>
#include <bob/star.hpp>
#include <bob/posterior.hpp>

typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;
using df_theta = std::map<std::string, double>;
using prior_fn = std::function<double(df_theta &)>;


/**
 * Return the percentile value of a vector.
 *
 * @param sorted_data  sorted vector of quantities
 * @param f            fraction of the distribution to report
 *
 * @return the percentile value
 */
double percentile(const std::vector<double>& sorted_data, double f=0.84){
    size_t n = sorted_data.size();
    size_t stride = 1;
    const double index = f * (n - 1) ;
    const size_t lhs = (int)index ;
    const double delta = index - lhs ;
    double result;

    if (n == 0)
        return 0.0 ;

    if (lhs == n - 1){
        result = sorted_data[lhs * stride] ;
    } else {
        result = (1 - delta) * sorted_data[lhs * stride] + delta * sorted_data[(lhs + 1) * stride] ;
    }
    return result;
}


/**
 * Find where maximum is reached in a vector
 */
template <typename T>
size_t argmax(const std::vector<T> & values){
    size_t argmax = 0;
    T value_max = values[0];
    for (size_t i=0; i < values.size(); ++i){
        if (values[i] > value_max){
            argmax = i;
            value_max = values[i];
        }
    }
    return argmax;
}


/**
 * The reporter is responsible for the export of the values to the final
 * catalog.
 *
 * It reads in the MCMC chains and extract statistics.
 */
class Reporter {

    public:
        Star star;
        Posterior lnp;
        std::vector<std::vector<double>> mcmc_trace;
        std::vector<std::string> which;

        std::map<std::string, double> summary; // keep track

        Reporter(const Star& star, const Posterior& lnp,
                 const std::vector<std::vector<double>>& mcmc_trace);
        Reporter(const Star& star, const Posterior& lnp,
                 const std::vector<std::vector<double>>& mcmc_trace,
		 const std::vector<std::string>& which);
        void add_trace_stats(const std::string& varname, const std::vector<double>& values, const size_t argbest = -1);
        std::vector<std::string> quantities;
        void to_csv(io::AsciiWriter& wt, bool header=true);

    private:
	void process();
};

Reporter::Reporter(const Star& star, const Posterior& lnp,
                   const std::vector<std::vector<double>>& mcmc_trace):
    star(star), lnp(lnp), mcmc_trace(mcmc_trace){

    this->which = {"logg", "logA", "Mbol",
		           "Gaia_MAW_G", "Gaia_MAW_BP_faint", "Gaia_MAW_RP",
                   "A_Gaia_MAW_G",  "A_Gaia_MAW_RP",
                   "A_2MASS_Ks", "A_Gaia_MAW_BP_bright",
                   "A_Gaia_MAW_BP_faint"};
    this->process();
}

Reporter::Reporter(const Star& star, const Posterior& lnp,
                   const std::vector<std::vector<double>>& mcmc_trace,
		   const std::vector<std::string>& which):
    star(star), lnp(lnp), mcmc_trace(mcmc_trace), which(which){
    this->process();
    }

void Reporter::process(){
    // copy star information (name, parallax, input recalibrated sed)
    // this->summary["source_id"] = this->star.name;
    this->summary["parallax"] = this->star.parallax;
    this->summary["parallax_sigma"] = this->star.parallax_error;
    for(size_t i = 0; i < this->star.sed_names.shape(0); ++i){
           this->summary[star.sed_names(i)] = star.sed_values[i];
           this->summary[star.sed_names(i) + "_sigma"] = star.sed_error[i];
    }

    // add lnp and prior
    std::vector<double> tmp_lnp(this->mcmc_trace.size());
    std::vector<double> tmp_lnlike(this->mcmc_trace.size());
    std::vector<std::size_t> shape = {mcmc_trace[0].size()};
    for(size_t k = 0; k < mcmc_trace.size(); ++k) {
        DMatrix theta_ = xt::adapt(mcmc_trace[k], shape);
        tmp_lnp[k] = this->lnp.lnposterior(theta_, star);
        tmp_lnlike[k] = this->lnp.lnlikelihood(theta_, star);
    }
    size_t argbest = argmax(tmp_lnp);
    this->add_trace_stats("lnp", tmp_lnp, argbest);
    this->add_trace_stats("lnlike", tmp_lnlike, argbest);

    // stats on the sampled dimensions
    std::vector<double> tmp(this->mcmc_trace.size());
    for (size_t dim = 0; dim < lnp.names.size(); ++dim){
        // copy the sample column
        for(size_t k = 0; k < mcmc_trace.size(); ++k) {
            tmp[k] = mcmc_trace[k][dim];
        }
        this->add_trace_stats(lnp.names[dim], tmp, argbest);
    }


    // Add other derived quantities
    if (!this->which.empty()){
	    DMatrix predictions = xt::empty<double>(
		    {mcmc_trace.size(), this->lnp.model->label_columns.shape(0)});

	    size_t ndim = mcmc_trace[0].size() - 2;
	    DMatrix theta = xt::empty<double>({ndim});
	    for(size_t k = 0; k < mcmc_trace.size(); ++k) {
            theta = xt::adapt(mcmc_trace[k], {ndim});
            DMatrix theta2d = xt::atleast_2d(theta);
            DMatrix pred = this->lnp.model->predict(theta2d);
            auto pred_view = xt::view(predictions, k, xt::all());
            pred_view = xt::view(pred, 0, xt::all());
	    }

	    for (const auto & name : this->which) {
			for (size_t dim = 0; dim < predictions.shape(1); ++dim){
				if (name == this->lnp.model->label_columns[dim]){
					for(size_t k = 0; k < predictions.shape(0); ++k){
						tmp[k] = predictions(k, dim);
					}
					this->add_trace_stats(name, tmp, argbest);
				}
			}
	    }
    }
}


/**
 * Get the statistics on a vector of values.
 *
 * @param varname   name of the quantity
 * @param values values to get the statistics
 * @param argbest location of the best value (or ref value)
 *
 *
 */
void Reporter::add_trace_stats(const std::string& varname, const std::vector<double>& values, const size_t argbest){

        std::vector<double> sorted_data(values.size());
        // get the data out
        for(size_t k = 0; k < values.size(); ++k) {
            sorted_data[k] = values[k];
        }
        //sort the data
        std::sort(sorted_data.begin(), sorted_data.end());

        this->summary[varname + "_min"] = sorted_data[0];
        this->summary[varname + "_max"] = sorted_data[sorted_data.size()-1];
        this->summary[varname + "_p16"] = percentile(sorted_data, 0.16);
        this->summary[varname + "_p25"] = percentile(sorted_data, 0.25);
        this->summary[varname + "_p50"] = percentile(sorted_data, 0.50);
        this->summary[varname + "_p75"] = percentile(sorted_data, 0.75);
        this->summary[varname + "_p84"] = percentile(sorted_data, 0.84);
        if (argbest < sorted_data.size()){
            this->summary[varname + "_best"] = values[argbest];
        }

        this->quantities.push_back(varname);
}


/**
 * Export the data to csv output
 *
 * @param wt       AsciiWriter instance
 * @param header   Set to also add a header
 */
void Reporter::to_csv(io::AsciiWriter& wt, const bool header){

    if (header){
        wt << "source_id";
        for(const auto& name: this->summary){
            wt << name.first;
        }
        ++wt;
    }

    wt << this->star.name;
    for(const auto& name: this->summary){
        wt << name.second;
    }
    ++wt;
}



/**
 * Nice representation of Reporter objects with cout
 */
std::ostream &operator<<(std::ostream &os, Reporter & reporter){

    os << std::endl
       << "Reporter object on "<< reporter.star.name << std::endl
       << "--------------------------------" << std::endl;

    size_t lenkey = 0;
    for (const auto & item: reporter.summary){
        lenkey = std::max(lenkey, item.first.size());
    }
    /*
    for (const auto & item: reporter.summary){
        std::cout << "    " << std::setw(lenkey) << item.first << " = " << item.second << std::endl;
    }
    */
    std::vector<std::string> stats = {"min", "max", "p16", "p25", "p50", "p75", "p84", "best"};

    lenkey -= 4;
    std::cout << std::setw(lenkey + 1) << " ";
    for (const auto & item: stats){
        std::cout << std::setw(11) << item << " ";
    }
    std::cout << std::endl;

    for (const auto & item: reporter.quantities){
        std::cout << std::setw(lenkey) << item << " ";

        for (const auto & what: stats){
            auto key = item + "_" + what;
            std::cout << std::setw(11) << reporter.summary[key] << " ";
        }
        std::cout << std::endl;
    }


    return os;
}
