/**
 * Let's try to code some BOB emulator model in CPP
 */
#ifndef Star_HH
#define Star_HH
#include <xtensor/xtensor.hpp>
#include <xtensor/xarray.hpp>
#include <string>
#include <iostream>
#include <iomanip>
#include <set>
#include <cmath>

// Bob libraries
#include <bob/helpers.hpp>


typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;



class Star {
    public:
        std::string name;
        double parallax;
        double parallax_error;
        xt::xarray<std::string> sed_names;
        DMatrix sed_values;
        DMatrix sed_error;

    Star(const std::string & name,
         const double parallax,
         const double parallax_error,
         const xt::xarray<std::string> & sed_names,
         const DMatrix & sed_values,
         const DMatrix & sed_error,
         const bool recalibrated=false);

    Star get_recalibrated();
    double get_sed_value(const std::string &band);
    double get_sed_error(const std::string &band);
    bool is_recalibrated() const;

    private:
        bool recalibrated = false;
};

Star::Star(const std::string & name,
           const double parallax,
           const double parallax_error,
           const xt::xarray<std::string> & sed_names,
           const DMatrix & sed_values,
           const DMatrix & sed_error,
           const bool recalibrated){

    this->name = name;
    this->parallax = parallax;
    this->parallax_error = parallax_error;
    this->sed_names = sed_names;
    this->sed_values = sed_values;
    this->sed_error = sed_error;
    this->recalibrated = recalibrated;
}

bool Star::is_recalibrated() const {
    return this->recalibrated;
}

double Star::get_sed_value(const std::string &band){
    for( size_t i=0; i<this->sed_names.size(); ++i){
        if (band == this->sed_names[i]){
            return this->sed_values[i];
        }
    }
    throw std::runtime_error("Band " + band + " not found.");
}

double Star::get_sed_error(const std::string &band){
    for( size_t i=0; i<this->sed_names.size(); ++i){
        if (band == this->sed_names[i]){
            return this->sed_error[i];
        }
    }
    throw std::runtime_error("Band " + band + " not found.");
}

/**
 * Returns the recalibrated values of the star as a star object
 */
Star Star::get_recalibrated(){
    if (this->recalibrated)
        return *this;

    std::string gmag_key = "Gaia_MAW_G";
    int gmag_index = 0;
    for( size_t i=0; i<this->sed_names.size(); ++i){
        if (gmag_key == this->sed_names[i]){
            gmag_index = i;
            break;
        }
    }
    // arrays are (deep-)copied by assignment to new variables.
    double gmag = this->get_sed_value(gmag_key);
    double parallax = this->parallax;
    double parallax_error = this->parallax_error;
    DMatrix sed_values = this->sed_values;
    DMatrix sed_error = this->sed_error;

    /* Parallax recalibration
     * ----------------------
     * Using Zinn et al. (2018), Lindegren (2018), and Lindegren et al. (2018)
     */
    if (gmag < 14.0){
        parallax += 0.05;  // mas
    } else if (gmag < 16.5){
        parallax += (0.1676 - 0.0084 * gmag);  // mas by linear interp.
    } else {
        parallax += 0.029;   // mas zeropoints on quasars
    }

    if (gmag < 11){
        parallax_error *= 1.2;
    } else if (gmag < 15){
        parallax_error *= (0.22 * gmag - 1.22);   // by interpolation
    } else {
        parallax_error *= (exp(15. - gmag) + 1.08);
    }

    /* SED recalibration
     * ------------------
     * Using Maiz-Apellaniz & Weiler (2018)
     */
    if (gmag < 6.){
        gmag += 0.0271 * (6 - gmag);
    } else if (gmag < 16.) {
        gmag += -0.0032 * (gmag - 6);
    } else {
        gmag += -0.032;
    }
    sed_values[gmag_index] = gmag;

    // SED uncertainties
    sed_error = xt::clip(sed_error, 1e-2, 100.);
    sed_error[!xt::isfinite(sed_error)] = 1e-2;

    for (size_t i=0; i<this->sed_names.size(); ++i){
        if ((tolower(this->sed_names[i])).find("gaia") != std::string::npos){
            sed_error[i] += 0.05;
        }
    }

    Star new_star(this->name, parallax, parallax_error,
                  this->sed_names, sed_values, sed_error,
                  true);
    return new_star;
}

/**
 * Nice representation of Star objects with cout
 */
std::ostream &operator<<(std::ostream &os, const Star &star) {

    size_t nchar = std::strlen("parallax: ");
    for(size_t i = 0; i < star.sed_names.shape(0); ++i){
        nchar = fmax(nchar, star.sed_names[i].size());
    }

    os << std::endl
       << "Star object " + star.name << std::endl
       << "--------------------------------" << std::endl;
    if (star.is_recalibrated())
        os << "(recalibrated)" << std::endl;
    os << std::setw(nchar) << std::right
       << "parallax" << ": " << star.parallax
       << " (+/- " << star.parallax_error << ")" << std::endl;
    for(size_t i = 0; i < star.sed_names.shape(0); ++i){
        os << std::setw(nchar) << std::right
           << star.sed_names(i) << ": " << star.sed_values[i]
           << " (+/- " << star.sed_error[i] << ")" << std::endl;
    }
    return os << std::endl;
}

#endif // Star_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
