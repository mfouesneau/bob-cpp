/**
 * Let's try to code some BOB emulator model in CPP
 */
#ifndef POSTERIOR_HH
#define POSTERIOR_HH
#include <exception>
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xmath.hpp>
#include <xtensor/xadapt.hpp>
#include "xtensor/xindex_view.hpp"
#include <xtensor-blas/xlinalg.hpp>
#include <xtensor/xmanipulation.hpp>
#include <xtensor-io/xhighfive.hpp>
#include <highfive/H5Easy.hpp>
#include <string>
#include <iostream>
#include <map>
#include <set>
// #include <math.h>
#include <cmath>

// Bob libraries
#include <pbar.hpp>
#include <bob/helpers.hpp>
#include <bob/polynomialregressionmodel.hpp>
#include <bob/emulator/base.hpp>
#include <bob/star.hpp>
#include <bob/vaextable.hpp>
#include <bob/datacatalog.hpp>
#include <bob/distributions.hpp>
#include <bob/imf.hpp>

typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;
using df_theta = std::map<std::string, double>;
using prior_fn = std::function<double(df_theta &)>;


/**
 * Convert vectors of names and values to named structure.
 *
 * @param names   names of the fields
 * @param values  values for each field
 * @return structure
 */
df_theta to_dataframe(const xt::xarray<std::string>& names,
                      const DMatrix& values) {
    std::map<std::string, double> df;
    for (int i = 0; i < values.shape(0); ++i) {
        df[names(i)] = values(i);
    }
    return df;
}

/**
 * Convert vectors of names and values to named structure.
 *
 * @param names   names of the fields
 * @param values  values for each field
 * @return structure
 */
df_theta to_dataframe(const xt::xarray<std::string>& names,
                      const std::vector<double>& values) {
    std::map<std::string, double> df;
    for (int i = 0; i < values.size(); ++i) {
        df[names(i)] = values[i];
    }
    return df;
}

/**
 * Nice representation of df_theta objects
 */
std::ostream &operator<<(std::ostream &os, const df_theta &df) {

    size_t nchar = 0;
    for (auto& val: df){
        nchar = fmax(nchar, val.first.size());
    }

    os << "DataFrame map<string, double>:\n"
       << "--------------------------------\n";
    for (auto& val: df){
        os << std::setw(nchar) << std::right
           << val.first  << " = " << val.second << std::endl;
    }
    return os << std::endl;
}

/**
 * Definition of the posterior including distance and photometric Jitter.
 */
class Posterior {

    public:
        Emulator::Base* model = NULL;
        xt::xarray<std::string> names;
        size_t ndim;
        xt::xarray<std::string> phot_columns;

        Posterior(Emulator::Base* model, xt::xarray<std::string> phot_columns);
        double lnlikelihood(const DMatrix & theta, const Star & star);
        double lnprior(const DMatrix & theta);
        std::map<std::string, double> theta_to_map(const DMatrix & theta);
        std::vector<prior_fn> priors;
        void set_priors(const std::vector<prior_fn>& priors);
        double lnposterior(const DMatrix & theta, const Star & star);

        DMatrix get_model_theta(const DMatrix & theta);
        DMatrix predict_observed_sed(const DMatrix & theta);
        DMatrix predict_observed_sed(const std::vector<double>& theta);

    private:
        xt::xarray<size_t> phot_idx;

        DMatrix predict_model_sed(DMatrix& model_theta, double dmod=0.);

};

/**
 * Constructor.
 *
 * @param model          emulator model
 * @param phot_columns   which sed columns to use (e.g, mapping[:, 0])
 */
Posterior::Posterior(Emulator::Base* model, xt::xarray<std::string> phot_columns){
    this->model = model;
    this->phot_columns = phot_columns;
    this->names = this->model->feature_columns;
    xt::xarray<std::string> other_names = {"dmod", "log10jitter"};
    this->names = xt::hstack(xt::xtuple(this->names, other_names));
    this->ndim = this->names.shape(0);

    // map the photometric inputs to the predicted dimensions
    this->phot_idx = xt::zeros<size_t>({this->phot_columns.shape(0)});
    for (size_t i = 0; i < this->phot_columns.shape(0); ++i){
        for (size_t j = 0; j < this->model->label_columns.shape(0); ++j){
            if (this->phot_columns[i] == this->model->label_columns[j]){
                this->phot_idx[i] = j;
                break;
            }
        }
    }
}


/**
 * Set priors.
 *
 * Priors apply to the named map of theta using the feature names
 * a typical prior function is [](const df_theta& df)( return 1./ df['A0']; )
 *
 * The function return p(X) and the final prior p(theta) = product(p_k(theta))
 *
 * @param priors   a list of prior functions to apply;
 */
void Posterior::set_priors(const std::vector<prior_fn>& priors){
    this->priors = priors;
}

/**
 * Returns an SED prediction from a given infered feature vector.
 *
 * @param model_theta   parameters that only refer to the emulator interaction
 * @param dmod          to put the SED at a given distance
 *
 * @return SED vector in order of this->phot_columns
 */
DMatrix Posterior::predict_model_sed(DMatrix& model_theta, const double dmod){

    DMatrix model_sed = xt::ones<double>({this->phot_idx.shape(0)});
    try {
        DMatrix model_pred = this->model->predict(model_theta);
        for (int i = 0; i < this->phot_idx.shape(0); ++i){
            model_sed[i] = model_pred(this->phot_idx[i]);
        }
        model_sed += dmod;
    } catch (const std::exception& e) {
        // std::cout << "ERROR: \n" <<  e.what() << "\n";
        // std::cout << "DEBUG: model_theta = " << model_theta << "\n";
        // std::cout << "DEBUG: model_sed = " << model_sed << "\n";
        model_sed.fill(std::numeric_limits<double>::quiet_NaN());
    }
    return model_sed;
}

/**
 * Get data into a named structure.
 *
 * @param theta   model parameters
 *
 * @return map of the vector
 */
std::map<std::string, double> Posterior::theta_to_map(const DMatrix & theta){
    std::map<std::string, double> df;
    for (int i = 0; i < theta.shape(0); ++i){
        df[this->names(i)] = theta(i);
    }
    return df;
}


/**
 * Get the part of the parameters linked to the physical model.
 *
 * @param theta   model parameters
 *
 * @return model_theta where theta = [model_theta] + [dmod, log10jitter]
 */
DMatrix Posterior::get_model_theta(const DMatrix & theta){
    size_t n_theta = theta.shape(0);
    return xt::view(xt::atleast_2d(theta),
                    xt::all(),
                    xt::range(_, n_theta - 2));
}


/**
 * Predict the model sed from the parameters.
 *
 * @param theta input parameters of the posterior
 * @return model_sed sed prediction at the requested distance
 */
DMatrix Posterior::predict_observed_sed(const DMatrix & theta){
    size_t n_theta = theta.shape(0);
    DMatrix model_theta = xt::view(xt::atleast_2d(theta),
	 	    		               xt::all(),
                                   xt::range(_, n_theta - 2));
    double dmod = theta(n_theta - 2);

    return this->predict_model_sed(model_theta, dmod);
}

/**
 * Predict the model sed from the parameters.
 *
 * @param theta input parameters of the posterior
 * @return model_sed sed prediction at the requested distance
 */
DMatrix Posterior::predict_observed_sed(const std::vector<double>& theta){
    std::vector<std::size_t> shape = {theta.size()};
    DMatrix theta_ = xt::adapt(theta, shape);
    return this->predict_observed_sed(theta_);
}


/**
 * Log-Likelihood definition.
 *
 * A normal distribution centered on the model SED(theta) where the
 * model noise is the observed noise + jitter.
 *
 * @param theta   model parameters
 * @param star    observed star (or recalibrated)
 *
 * @return lnp(star | theta)
 */
double Posterior::lnlikelihood(const DMatrix & theta, const Star & star){
    // size_t n_phot = this->phot_idx.shape(0);
    size_t n_theta = theta.shape(0);
    DMatrix model_theta = xt::view(xt::atleast_2d(theta),
		    		   xt::all(), xt::range(_, n_theta - 2));
    double dmod = theta(n_theta - 2);
    double log10jitter = theta(n_theta - 1);
    double jitter = std::pow(10, log10jitter);

    // Photometric likelihood
    DMatrix model_sed = this->predict_model_sed(model_theta, dmod);
    DMatrix phot_var = xt::pow(star.sed_error, 2) + std::pow(jitter, 2);

    bool crazy_sed = false;
    for(double val: model_sed){
        if ((val > 30.) || (val < -20)) {
            crazy_sed = true;
        }
    }
    // if (crazy_sed){
    //    return -std::numeric_limits<double>::infinity();
    //}

    /*
    std::cout << "DEBUG ----- \n"
              << to_dataframe(this->phot_columns, model_sed)
              << "\n";
    */

    auto ind = xt::isfinite(star.sed_values) && (xt::isfinite(star.sed_error));
    double lnp_phot = 0;
    for (size_t i = 0; i < star.sed_values.shape(0); ++i){
        if (ind[i]){
            lnp_phot += normal::logpdf(star.sed_values[i],
                                       model_sed[i],
                                       phot_var[i]);
        }
    }

    // parallax likelihood
    double model_parallax = std::pow(10, 2. - 1. / 5. * dmod);
    double lnp_varpi = normal::logpdf(star.parallax,
                                      model_parallax,
                                      std::pow(star.parallax_error, 2));
    return lnp_phot + lnp_varpi;
}


/**
 * Log-prior definition.
 * @param theta   model parameters
 *
 * @return lnp(theta)
 */
double Posterior::lnprior(const DMatrix & theta){

    double lnprior = 0.0;
    df_theta df = this->theta_to_map(theta);

    double val;
    for (const auto & priorfn : this->priors){
        try {
            val = priorfn(df);
            if (std::isfinite(val)){
                lnprior += std::log(val);
            } else {
                lnprior += -std::numeric_limits<double>::infinity();
            }
        } catch (const std::out_of_range& e)  {
            // ignore
        }
    }
    return lnprior;
}

/**
 * Defines the log-posterior.
 *
 * @param theta   model parameters
 * @param star    observed star (or recalibrated)
 */
double Posterior::lnposterior(const DMatrix & theta, const Star & star){
    double lnp = this->lnprior(theta);
    if (!std::isfinite(lnp)){
        return -std::numeric_limits<double>::infinity();
    }

    lnp += this->lnlikelihood(theta, star);
    if (!std::isfinite(lnp)){
        return -std::numeric_limits<double>::infinity();
    }
    return lnp;
}

/**
 * Nice representation of Posterior objects with cout
 */
std::ostream &operator<<(std::ostream &os, const Posterior &lnp) {

    os << std::endl
       << "Posterior object " << std::endl
       << "--------------------------------" << std::endl
       << lnp.ndim << " infered dimensions: " << std::endl
       << lnp.names << std::endl
       << "using " << lnp.priors.size() << " prior functions" << std::endl
       << "and from " << lnp.phot_columns.shape(0) << " photometric dimensions: "
       << std::endl
       << lnp.phot_columns << std::endl;

    return os;
}


/**
 * A wrapper to the lnp object that works with emcee.
 *
 * This wrapper makes sure the code calls the lnp.posterior during the sampling.
 * `lnp.posterior` is not const so the partial function does not work directly
 * on it. This wrapper does the trick.
 *
 * Additionally,  Emcee (as it is written now) needs a std::vector object, while
 * the posterior and related calculations are done with xt::array. While this is
 * not a priority to update emcee code this wrapper also takes care of the
 * interface and converts the emcee proposals to xt:array objects (DMatrix)
 *
 * @param theta  proposal vector
 * @param lnp    the posterior object
 * @param star   the star to get the data from
 *
 * @return the lnposterior value lnp(theta | star, models)
 * */
double lnp_wrapper(const std::vector<double>& theta,
                   Posterior& lnp,
                   const Star& star){
    std::vector<std::size_t> shape = { theta.size()};
    DMatrix theta_ = xt::adapt(theta, shape);

    return lnp.lnposterior(theta_, star);
}
#endif // POSTERIOR_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
