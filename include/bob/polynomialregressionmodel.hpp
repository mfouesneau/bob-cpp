/**
 * Defines polynomial regression model
 */
#ifndef PolynomialRegressionModel_HH
#define PolynomialRegressionModel_HH
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor-blas/xlinalg.hpp>
#include <highfive/H5Easy.hpp>
#include <string>

// Bob libraries
#include <pbar.hpp>
#include <bob/helpers.hpp>


typedef double DType;
// linalg package doesn't support dynamic layouts
using Matrix = xt::xarray<DType, xt::layout_type::row_major>;
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;

/**
 * Generate polynomial and interaction features.
 *
 * Generate a new feature matrix consisting of all polynomial combinations of the features with degree less than
 * or equal to the specified degree.
 *
 * For example, if an input sample is two dimensional and of the form [a, b], the degree-2 polynomial features are
 * [1, a, b, a^2, ab, b^2].
 *
 * Note: This version returns identical outputs as the python `sklearn.preprocessing.PolynomialFeatures`
 *
 * @tparam T                data type of input
 * @param input data        vector to expand
 * @param degree            The degree of the polynomial features.
 * @param interaction_only  If true, only interaction features are produced: features that are products of at most
 *                          degree distinct input features (so not x[1] ** 2, x[0] * x[2] ** 3, etc.).
 * @param include_bias      If True, then include a bias column, the feature in which all polynomial powers are zero
 *                          (i.e. a column of ones - acts as an intercept term in a linear model).
 * @return Transformed array.
 */
template <class T>
std::vector<T> polynomialFeatures( const std::vector<T>& input,
                                   unsigned int degree, bool interaction_only, bool include_bias )
{
    std::vector<T> features = input;
    std::vector<T> prev_chunk = input;
    std::vector<size_t> indices( input.size() );
    std::iota( indices.begin(), indices.end(), 0 );

    for ( int d = 1 ; d < degree ; ++d )
    {
        // Create a new chunk of features for the degree d:
        std::vector<T> new_chunk;
        // Multiply each component with the products from the previous lower degree:
        for ( size_t i = 0 ; i < input.size() - ( interaction_only ? d : 0 ) ; ++i )
        {
            // Store the index where to start multiplying with the current component at the next degree up:
            size_t next_index = new_chunk.size();
            for ( auto coef_it = prev_chunk.begin() + indices[i + ( interaction_only ? 1 : 0 )] ;
                    coef_it != prev_chunk.end() ; ++coef_it )
            {
                new_chunk.push_back( input[i] ** coef_it );
            }
            indices[i] = next_index;
        }
        // Extend the feature vector with the new chunk of features:
        features.reserve( features.size() + std::distance( new_chunk.begin(), new_chunk.end() ) );
        features.insert( features.end(), new_chunk.begin(), new_chunk.end() );

        prev_chunk = new_chunk;
    }
    if ( include_bias )
        features.insert( features.begin(), 1 );

    return features;
}


class PolynomialRegressionModel {

public:
		DMatrix coef;
        IMatrix powers;
		std::string name;
        DMatrix x_mean;
        DMatrix x_std;
        DMatrix x_min;
        DMatrix x_max;
        DMatrix y_mean;
        DMatrix y_std;
        DMatrix y_min;
        DMatrix y_max;
		xt::xarray<std::string> features;
		xt::xarray<std::string> labels;
        long n_samples = 0;
		

		PolynomialRegressionModel(){};
		PolynomialRegressionModel(
			const DMatrix coef,
			const IMatrix powers,
			const std::string name = "model");

		PolynomialRegressionModel(const PolynomialRegressionModel& model);

		DMatrix get_center();
		DMatrix transform(const DMatrix& indata);
		DMatrix predict(const DMatrix& indata);
        bool is_stable();
        IMatrix is_stable_prediction(const DMatrix& ypred);
};

/**
 * Constructor
 * 
 * @param coef: (n_components, n_labels)
 *				regression coefficients on the transformed data
 * @param powers: (n_components, n_features)
 *                polynomial expension power definition
 *                e.g., [1, 2, 0, 0] means x_0 * x_1 ** 2
 * @param name: name of the model
 */
PolynomialRegressionModel::PolynomialRegressionModel(
			const DMatrix coef,
			const IMatrix powers,
			const std::string name){

		this->coef = coef;
		this->powers = powers;
		this->name = name;
}

/** 
 * Constructor by shallow copy 
 */
PolynomialRegressionModel::PolynomialRegressionModel(
			const PolynomialRegressionModel& model){

		this->coef = model.coef;
		this->powers = model.powers;
		this->name = model.name;
		this->x_mean = model.x_mean;
		this->x_std = model.x_std;
		this->x_min = model.x_min;
		this->x_max = model.x_max;
		this->y_mean = model.y_mean;
		this->y_std = model.y_std;
		this->y_min = model.y_min;
		this->y_max = model.y_max;
		this->n_samples = model.n_samples;
		this->labels = model.labels;
		this->features = model.features;
}


/**
 * Get the central value of that bin
 * 
 * Note that this could be away from the mean if the density in that bin
 * and neighbors is far from uniform.
 */
DMatrix PolynomialRegressionModel::get_center(){
		return 0.5 * (this->x_min + this->x_max);
}


/**
 * Returns a stability quality flag for the model.
 *
 * @return stability quality
 */
bool PolynomialRegressionModel::is_stable(){
    return (this->n_samples > this->powers.shape(0));
}

/**
 * Returns a stability quality flag for the model.
 *
 * @return stability quality
 */
IMatrix PolynomialRegressionModel::is_stable_prediction(const DMatrix& ypred){
	auto out_shape = std::vector<size_t>{ypred.shape(0)} ;
    IMatrix flags = xt::ones<int>(out_shape);
    for (size_t i=0; i<flags.shape(0); ++i){
        auto yvals = xt::view(ypred, i, xt::all());
        for (size_t dim=0; dim < ypred.shape(0); ++dim){
            if ((yvals[dim] < this->y_min[dim]) || (yvals[dim] > this->y_max[dim])){
                flags[i] = 0;
                break; // no need to check more
            }
        }
        flags[i] = 1;
    }
    return flags;
}

/**
 * Expand the indata into the polynomial features
 * 
 * @param indata: (n_data, n_features) input indata
 *
 * @return values (n_data, n_components) transformed indata
 */
DMatrix PolynomialRegressionModel::transform(const DMatrix& indata){

	auto data_ = xt::atleast_2d(indata);
	auto out_shape = std::vector<size_t>{this->powers.shape(0), data_.shape(0)};
    DMatrix values = xt::ones<DType>(out_shape);

	for(size_t row_k=0; row_k < this->powers.shape(0); ++row_k){
			auto element = xt::row(this->powers, row_k);
			auto values_view = xt::view(values, row_k, xt::all());
			for(size_t feat=0; feat < element.shape(0); ++feat){
					auto data_feat = xt::view(data_, xt::all(), feat);
					auto temp_row = xt::pow(data_feat, element(feat));
					values_view = xt::eval(values_view * temp_row);
			}
	}
	return xt::transpose(values);
}

/**
 * Predict using the model
 *
 * @param indata:  (n_samples, n_features) Samples.
 *
 * @return C : (n_samples,) Returns predicted values.
 */
DMatrix PolynomialRegressionModel::predict(const DMatrix& indata){
	DMatrix transformed_data = this->transform(indata);
	return xt::linalg::dot(transformed_data,
						   xt::transpose(this->coef));
}


/**
 * Load a model from an hdf5 file
 *
 * @param fname file containing the model
 * @param key   key in which the model is stored
 *
 * @return a polynomial regression model
 */
PolynomialRegressionModel from_hdf5(
	const HighFive::File& file,
	const std::string key
	){
	std::string path_root = "/models/" + key;
	auto coef = H5Easy::load<DMatrix>(file, path_root + "/coef");
	auto pows = H5Easy::load<DMatrix>(file, path_root + "/model_powers");
	PolynomialRegressionModel new_model = PolynomialRegressionModel(coef, pows, key);
	new_model.x_mean = H5Easy::load<DMatrix>(file, path_root + "/x_mean");
	new_model.x_std = H5Easy::load<DMatrix>(file, path_root + "/x_std");
	new_model.x_min = H5Easy::load<DMatrix>(file, path_root + "/x_min");
	new_model.x_max = H5Easy::load<DMatrix>(file, path_root + "/x_max");
	new_model.y_mean = H5Easy::load<DMatrix>(file, path_root + "/y_mean");
	new_model.y_std = H5Easy::load<DMatrix>(file, path_root + "/y_std");
	new_model.y_min = H5Easy::load<DMatrix>(file, path_root + "/y_min");
	new_model.y_max = H5Easy::load<DMatrix>(file, path_root + "/y_max");
	new_model.n_samples = H5Easy::load<IMatrix>(file, path_root + "/model_shape")[0];
	// new_model.labels = H5Easy::load<xt::xarray<std::string>>(file, path_root + "/model_labels");
	// new_model.features = H5Easy::load<xt::xarray<std::string>>(file, path_root + "/model_features");
	// auto dataset = file.getDataSet(path_root + "/model_features");
	return new_model;
}

/**
 * Load a model from an hdf5 file
 *
 * @param fname file containing the model
 * @param key   key in which the model is stored
 *
 * @return a polynomial regression model
 */
PolynomialRegressionModel from_hdf5(
	const std::string fname,
	const std::string key
	){
    HighFive::File file(fname, HighFive::File::ReadOnly);
	return from_hdf5(file, key);
}


/**
 * Get key associated with a model file 
 */
std::string parse_model_key(const std::string& filename){
    int last_dir = filename.find_last_of("/");
    int extension = filename.find_last_of(".");
    return filename.substr(last_dir + 1, extension - last_dir - 1);
}


/**
 * Generate a collection of PolynomialRegressionModel objects similar to a
 * dictionary
 *
 * @param path      where to find the files
 * @param pattern   what kind of pattern to filter files
 *
 * @return collection of models
 */
auto collection_from_files(const std::string& path, 
                           const std::string& pattern=".*\\.hdf5"){

    std::map<std::string, PolynomialRegressionModel> collection;

    auto files = list_dir(path, pattern);

    // PolynomialRegressionModel model;
    std::string key;

    PBar pb(files.size());
    for (auto & entry : files){
        ++pb;
        key = parse_model_key(entry);
        PolynomialRegressionModel model = from_hdf5(entry, key);
        collection.insert(std::make_pair(key, model));
    }

    return collection;
}

/**
 * Generate a collection of PolynomialRegressionModel objects similar to a
 * dictionary
 *
 * @param path      where to find the files
 * @param pattern   what kind of pattern to filter files
 *
 * @return collection of models
 */
auto collection_from_single_file(const HighFive::File& file){

    std::map<std::string, PolynomialRegressionModel> collection;
	
    HighFive::Group data = file.getGroup("/models");
    auto model_keys = data.listObjectNames();

    PBar pb(model_keys.size());
    for (const auto & key : model_keys){
        ++pb;
        PolynomialRegressionModel model = from_hdf5(file, key);
        collection.insert(std::make_pair(key, model));
    }
    return collection;
}

/**
 * Generate a collection of PolynomialRegressionModel objects similar to a
 * dictionary
 *
 * @param path      where to find the files
 * @param pattern   what kind of pattern to filter files
 *
 * @return collection of models
 */
auto collection_from_single_file(const std::string& path){

    HighFive::File file(path, HighFive::File::ReadOnly);
    return collection_from_single_file(file);
}

/**
 * Nice representation of Star objects with cout
 */
std::ostream &operator<<(std::ostream &os, const PolynomialRegressionModel &model) {
    os << std::endl
       << "PolynomialRegressionModel object " + model.name << std::endl
       << "--------------------------------------------" << std::endl
       << "x_mean = " << model.x_mean  << "\n"
       << "x_std = " << model.x_std  << "\n"
       << "x_min = " << model.x_min  << "\n"
       << "x_max = " << model.x_max  << "\n"
       << "y_mean = " << model.y_mean  << "\n"
       << "y_std = " << model.y_std  << "\n"
       << "y_min = " << model.y_min  << "\n"
       << "y_max = " << model.y_max  << "\n"
       << "n_samples = " << model.n_samples  << "\n"
       << "\n"
       << "coef = " << model.coef << "\n"
       << "pows = " << model.powers << "\n";
    return os << std::endl;
}


#endif // PolynomialRegressionModel_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
