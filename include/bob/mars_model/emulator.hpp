/**
 * @file emulator.hpp
 * @author Morgan Fouesneau
 * @brief  Definition of the emulator based on MARS modeling
 * @version 0.1
 * @date 2020-10-09
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once
#include "bob/emulator/base.hpp"
#include "bob/mars_model/mars_model_definition.hpp"

namespace Emulator {

    /**
     * Multivariate adaptive regression with splines model
     *
     * MARS - name is trademarked from Standford
     */
    class Mars: public Base {

        public:
            std::vector<mars_model::model_fn> models;

            Mars();
            DMatrix predict(DMatrix& Xin);
            std::vector<double> predict(std::vector<double>& x);
    };

    /**
     * Constructor.
     */
    Mars::Mars() : Base() {
        this->name = "MarsEmulator";
        std::vector<std::size_t> feature_shape = { mars_model::feature_names.size() };
        this->feature_columns = xt::adapt(mars_model::feature_names, feature_shape);
        std::vector<std::size_t> label_shape = { mars_model::label_names.size() };
        this->label_columns = xt::adapt(mars_model::label_names, label_shape);
        this->models = mars_model::model;
    }

    /**
     * Predict using the model.
     *
     * @param x   (n_features, ) single input sample
     *
     * @return (n_labels, ) Returns predicted values.
     */
    std::vector<double> Mars::predict(std::vector<double>& x){
        size_t n_labels = mars_model::label_names.size();
        size_t n_features = this->feature_columns.size();

        if (x.size() != n_features){
            throw std::range_error("Input vector of wrong length.");
        }

        bool valid = true;
        for (size_t i = 0; i < n_features; ++i){
            if ((mars_model_limits::features_min[i] > x[i]) ||
                (mars_model_limits::features_max[i] < x[i])){
                    valid = false;
                    break;
                }
        }

        std::vector<double> prediction(n_labels);
        if (!valid){
            std::fill(prediction.begin(), prediction.end(),
                    std::numeric_limits<double>::quiet_NaN());
            return prediction;
        }
        for (size_t i = 0; i < n_labels; ++i){
            auto fn = this->models[i];
            prediction[i] = fn(x);
        }
        return prediction;
    }

    /**
     * Predict using the model.
     *
     * @param x   (n_samples, n_features) Samples.
     *
     * @return (n_samples, n_labels) Returns predicted values.
     */
    DMatrix Mars::predict(DMatrix& x){
        auto xin_ = xt::atleast_2d(x);
        size_t n_x = xin_.shape(0);
        size_t n_dim = xin_.shape(1);
        size_t n_features = this->feature_columns.size();
        if (n_dim != n_features){
            throw std::range_error("Input vector of wrong length.");
        }
        DMatrix pred = xt::zeros<double>({n_x, this->label_columns.shape(0)});
        for (size_t i=0; i < n_x; ++i){
            auto xi = xt::view<DMatrix>(xin_, i, xt::all());
            auto predview = xt::view(pred, i, xt::all());
            predview.fill(std::numeric_limits<double>::quiet_NaN());
            std::vector<double> xprime(xi.begin(), xi.end());
            auto temp = this->predict(xprime);
            predview = xt::adapt(temp, {temp.size()});
        }
        return pred;
    }
}; // namespace Emulator