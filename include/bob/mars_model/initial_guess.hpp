/**
 * @file initial_guess.hpp
 * @author Morgan Fouesneau
 * @brief Definition the Initial guess of a MARS Model.
 * @version 0.1
 * @date 2020-10-09
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once
#include <string>
#include <iostream>
#include <math.h>
#include <cmath>
#include <cstdio>
#include <vector>
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xarray.hpp>
#include "bob/mars_model/mars_model_definition.hpp"
#include "bob/posterior.hpp"

typedef double DType;
// linalg package doesn't support dynamic layouts
using shape_type = std::vector<std::size_t>;


class InitialGuess {

    public:

        InitialGuess(const xt::xarray<std::string>& phot_columns);
        std::vector<double> predict(const std::vector<double>& sed,
                                    const std::vector<double>& sederr,
                                    const double dmod=0.);
        std::vector<double> predict(const DMatrix& sed,
                                    const DMatrix& sederr,
                                    const double dmod);
        std::vector<double> predict(const DMatrix& sed,
                                    const DMatrix& sederr,
                                    const double dmod,
                                    Posterior& lnp);
        std::vector<double> predict(const std::vector<double>& sed,
                                    const std::vector<double>& sederr,
                                    const double dmod,
                                    Posterior& lnp);

    private:
        std::vector<mars_model::model_fn> get_models() {return mars_model::model;};
        std::vector<std::vector<double>> get_X () { return mars_model_guess::reference_X;};
        std::vector<std::vector<double>> Y;

};

/**
 * @brief Construct a new Initial Guess:: Initial Guess object
 *
 * @param phot_columns ordered list of sed names to construct the photometric prediction.
 */
InitialGuess::InitialGuess(const xt::xarray<std::string>& phot_columns){

    // match model::labels to phot_columns
    auto labels = mars_model::label_names;
    std::vector<size_t> use_models;
    bool phot_found = false;
    for(const std::string& name: phot_columns){
        phot_found = false;
        for (size_t k=0; k<labels.size(); ++k){
            if (labels[k].compare(name) == 0){
                use_models.push_back(k);
                phot_found = true;
                break;
            }
        }
        if (!phot_found){
            throw std::runtime_error("photometric column " + name + " not predicted by the model");
        }
    }

    // precompute the Y predictions
    auto models = this->get_models();
    auto X = this->get_X();
    size_t n_labels = models.size();
    size_t n_refs = X.size();

    // initiate allocation
    this->Y = std::vector<std::vector<double>> ( n_refs, std::vector<double> (phot_columns.size()));

    for (size_t i=0; i < n_refs; ++i){
        std::vector<double> xi = X[i];
        for (size_t j=0; j < use_models.size(); ++j){
            this->Y[i][j] = models[j](xi);
        }
    }
}

/**
 * @brief Predicts the best guess given the reference SEDs
 *
 * Find the nearest SED and returns its parameter vector.
 * The sed uncertainties are used to filter the input sed dimensions.
 *
 * @param sed       input sed
 * @param sederr    sed uncertainties
 * @param dmod      distance modulus
 * @return std::vector<double>
 */
std::vector<double> InitialGuess::predict(const std::vector<double>& sed,
                                          const std::vector<double>& sederr,
                                          const double dmod){

    size_t nphot = this->Y[0].size();
    double best_rms = 1e18;
    double current_rms;
    size_t best_match;
    for (size_t i=0; i < this->Y.size(); ++i){
        current_rms = 0;
        auto Yi = this->Y[i];
        for (size_t dim=0; dim < nphot; ++dim){
            if ((std::isfinite(sed[dim])) & (std::isfinite(sederr[dim])) ){
                current_rms += (sed[dim] - Yi[dim] - dmod) * (sed[dim] - Yi[dim] - dmod);
            }
        }
        if (current_rms < best_rms){
            best_match = i;
            best_rms = current_rms;
        }
    }
    return this->get_X()[best_match];
}

/**
 * @brief Predicts the best guess given the reference SEDs
 *
 * Find the nearest SED and returns its parameter vector.
 * The sed uncertainties are used to filter the input sed dimensions.
 *
 * @param sed       input sed
 * @param sederr    sed uncertainties
 * @param dmod      distance modulus
 * @param lnp       posterior object
 * @return std::vector<double>
 */
std::vector<double> InitialGuess::predict(const std::vector<double>& sed,
                                          const std::vector<double>& sederr,
                                          const double dmod,
                                          Posterior& lnp){

    size_t nphot = this->Y[0].size();
    double best_rms = 1e18;
    double current_rms;
    size_t best_match;
    std::vector<double> theta (lnp.ndim);
    theta[lnp.ndim - 1] = -5;
    theta[lnp.ndim - 2] = dmod;

    DMatrix vtheta = xt::adapt(theta, {theta.size()});

    for (size_t i=0; i < this->Y.size(); ++i){
        current_rms = 0;
        auto Yi = this->Y[i];
        for (size_t dim=0; dim < nphot; ++dim){
            if ((std::isfinite(sed[dim])) & (std::isfinite(sederr[dim])) ){
                current_rms += (sed[dim] - Yi[dim] - dmod) * (sed[dim] - Yi[dim] - dmod);
            }
        }
        auto X = this->get_X()[i];
        for (size_t k=0; k < lnp.ndim-2 ; ++k) {vtheta[k] = X[k];};
        current_rms += -2 * lnp.lnprior(vtheta);
        if (current_rms < best_rms){
            best_match = i;
            best_rms = current_rms;
        }
    }
    return this->get_X()[best_match];
}

/**
 * @brief Predicts the best guess given the reference SEDs
 *
 * Find the nearest SED and returns its parameter vector.
 * The sed uncertainties are used to filter the input sed dimensions.
 *
 * @param sed       input sed
 * @param sederr    sed uncertainties
 * @param dmod      distance modulus
 * @return std::vector<double>
 */
std::vector<double> InitialGuess::predict(const DMatrix& sed,
                                          const DMatrix& sederr,
                                          const double dmod){

    std::vector<double> sed_prime (sed.begin(), sed.end());
    std::vector<double> sederr_prime (sed.begin(), sed.end());
    return this->predict(sed_prime, sederr_prime, dmod);
}

/**
 * @brief Predicts the best guess given the reference SEDs
 *
 * Find the nearest SED and returns its parameter vector.
 * The sed uncertainties are used to filter the input sed dimensions.
 *
 * @param sed       input sed
 * @param sederr    sed uncertainties
 * @param dmod      distance modulus
 * @param lnp       posterior object
 * @return std::vector<double>
 */
std::vector<double> InitialGuess::predict(const DMatrix& sed,
                                          const DMatrix& sederr,
                                          const double dmod,
                                          Posterior& lnp){

    std::vector<double> sed_prime (sed.begin(), sed.end());
    std::vector<double> sederr_prime (sed.begin(), sed.end());
    return this->predict(sed_prime, sederr_prime, dmod, lnp);
}

/**
 * Nice representation of Emulator objects with cout
 */
std::ostream &operator<<(std::ostream &os, const InitialGuess& guess) {

    os << std::endl
       << "Mars Initial Guess object\n"
       << "--------------------------------" << std::endl
       << std::endl;
    return os << std::endl;
}