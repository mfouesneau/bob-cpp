/**
 * @file boundary2d.hpp
 * @author Morgan Fouesneau
 * @brief Definition of the 2D HRD prior
 * @version 0.1
 * @date 2020-10-09
 *
 * @copyright Copyright (c) 2020
 *
 */
#pragma once
#include <string>
#include <iostream>
#include <math.h>
#include <cmath>
#include <cstdio>
#include <vector>
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xadapt.hpp>
#include "bob/mars_model/mars_model_definition.hpp"

typedef double DType;
// linalg package doesn't support dynamic layouts
using Matrix = xt::xarray<DType, xt::layout_type::row_major>;
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;

/**
 * @brief Putting some clear constrains on a 2D space
 */
class Boundary2D {

    private:
        std::vector<double> x_coords;  // mask pixels along x
        std::vector<double> y_coords;  // mask pixels along y
        std::vector<std::vector<double>> mask;      // mask image <len(x), len(y)>
        size_t nx;
        double dx, xmin, xmax;
        size_t ny;
        double dy, ymin, ymax;

        void setup(const std::vector<std::vector<double>>& mask,
                            const std::vector<double>& x_coords,
                            const std::vector<double>& y_coords);

    public:
        Boundary2D();  // constructor
        Boundary2D(const std::vector<std::vector<double>>& mask,
                   const std::vector<double>& x_coords,
                   const std::vector<double>& y_coords);  // constructor
        void fit(const std::vector<std::vector<double>>& X,
                 double dyn=0.02, double smooth=0.2);
        std::vector<double> predict(const std::vector<std::vector<double>>& X);
        double predict(const std::vector<double>& X);
        double predict(const double x, const double y) const;
};

/**
 * @brief Construct a new Boundary 2D from ingredients
 *
 * @param mask        mask of the distribution
 * @param x_coords    coordinates along x
 * @param y_coords    coordinates along y
 */
Boundary2D::Boundary2D(const std::vector<std::vector<double>>& mask,
                       const std::vector<double>& x_coords,
                       const std::vector<double>& y_coords){
    this->setup(mask, x_coords, y_coords);
}

/**
 * @brief Setup the internal parameters
 *
 * @param mask      mask of the boundaries
 * @param x_coords  x coordinates of the pixels
 * @param y_coords  y coordinates of the pixels
 */
void Boundary2D::setup(const std::vector<std::vector<double>>& mask,
                       const std::vector<double>& x_coords,
                       const std::vector<double>& y_coords){
    this->x_coords = x_coords;
    this->y_coords = y_coords;
    this->mask = mask;
    this->nx = this->x_coords.size();
    this->ny = this->y_coords.size();
    this->dx = this->x_coords[1] - this->x_coords[0];
    this->dy = this->y_coords[1] - this->y_coords[0];
    this->xmin = this->x_coords[0];
    this->ymin = this->y_coords[0];
    this->xmax = this->x_coords[nx - 1];
    this->ymax = this->y_coords[ny - 1];
}

/**
 * @brief return the values associated with the {X[i]}
 *
 * @param X  many X = {x, y} positions
 * @return std::vector<double> mask values for each X[i]
 */
std::vector<double> Boundary2D::predict(const std::vector<std::vector<double>>& X){
    std::vector<double> results(X.size());
    for (size_t i=0; i < X.size(); ++i){
        results[i] = this->predict(X[i]);
    }
    return results;
}

/**
 * @brief return the value at the position X
 *
 * @param X   2d vector position
 * @return    value of the mask
 */
double Boundary2D::predict(const std::vector<double>& X){
    return this->predict(X[0], X[1]);
}

/**
 * @brief Return the mask value at x, y
 *
 * @param x    x-coordinate
 * @param y    y-coordinate
 * @return     value of the mask
 */
double Boundary2D::predict(const double x, const double y) const {

    // check within limits
    if ((x > this->xmax) || (x < this->xmin) || (y > this->ymax) || (y < this->ymin))
        return false;

    // position indices
    size_t indx = ((x - xmin) / dx);
    size_t indy = ((y - ymin) / dy);

    return this->mask[indx][indy];
}