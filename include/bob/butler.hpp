#include <string>
#include <iostream>
#include <set>
// provided libs
#include <xtensor/xarray.hpp>
// Bob libraries
#include <mfio.hpp>
#include <bob/helpers.hpp>


/**
 * Checks that the work was indeed done at the output level.
 *
 * .. note::
 *      "majordome" or butler is the person who speaks, makes arrangements, or
 *      takes charge for another
 */
class Butler {
    private:
        std::string filename;
        std::set<std::string> processed;

    public:
        Butler (const std::string& filename, const std::string& identifier_column);

        /**
         * Reports is the current name already exists in the output file
         * @param name    object identifier
         * @return true   if already done
         */
        bool is_done(std::string name) {return this->processed.count(name) > 0;};

        /**
         * Returns the filename of reference
         */
        std::string get_filename() {return this->filename;};
};


/**
 * @brief Construct a new Butler:: Butler object
 *
 * @param filename            result file to inspect
 * @param identifier_column   which column is the identifier
 */
Butler::Butler (const std::string& filename,
                  const std::string& identifier_column) :
                  filename(filename)
                  {
    if (utils::file_exists(this->filename)){
        if (io::count_lines(this->filename, "#") > 1){
            auto rd = io::AsciiReader(this->filename);
            rd.set_comment("#");
            rd.set_delimiter(",");
            rd.set_columns_from_names({identifier_column});

            std::string name;
            while(++rd){
                rd.get_next_value<std::string>(name);
                this->processed.insert(name);
                if (this->processed.count(name) > 1){
                    std::cout << name << " " << this->processed.count(name) << "\n";
                }
            }
        }
    }
}

/**
 * Nice representation of Butlers
 */
std::ostream &operator<<(std::ostream &os, Butler& niles) {
    os << " Butler inspecting " << niles.get_filename() << "\n";
    return os << std::endl;
}