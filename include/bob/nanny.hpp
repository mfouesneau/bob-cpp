/**
 * Let's try to code some BOB emulator model in CPP
 */
#pragma once
#include <string>
#include <iostream>
#include <cstdio>
#include <sys/stat.h>
// provided libs
#include <bob/helpers.hpp>


/**
 * class that monitors status of tasks.
 * It is a file based status of tasks to make sure things run completely and
 * nothing does twice the same work.
 */
class Nanny{
    public:
        std::string where;
        std::string taskname;

        Nanny(const std::string& task, const std::string& where="./nanny");
        void lock_task();
        void release_task();
        void clear_task();
        bool is_locked();
        bool is_released();

    private:
        std::string get_lockname();
        void check_where_exists();
        
};

/**
 * Constructor
 *
 * @param task    name of the task to register
 * @param where   where the registery is located (make sure the directory exists)
 *
 */
Nanny::Nanny(const std::string& task, const std::string& where): 
    where(where), taskname(task){ 
    this->check_where_exists();
}


/**
 * Attempts to create a directory for the registery.
 * I am not sure this works.
 */
void Nanny::check_where_exists(){
    if (utils::mkdir(this->where.c_str()) != 0)
    {
        std::cout << "I'm so sorry. I was not able to create the directory\n" ;
    }
}

/**
 * Returns the key filename without status
 */
std::string Nanny::get_lockname(){
    return this->where + "/" + this->taskname;
}

/**
 * Set the task to lock
 */
void Nanny::lock_task(){
    if (this->is_released()){
        throw std::runtime_error("Task " + this->taskname + " is already Release.");
    }

    std::string lock_filename = this->get_lockname() + ".lock";
    if (utils::file_exists(lock_filename)){
        throw std::runtime_error("Task " + this->taskname + " is already locked.");
    }
    std::ofstream output(lock_filename);
}

/**
 * Set the task to released
 */
void Nanny::release_task(){
    std::string lock_filename = this->get_lockname() + ".lock";
    std::string release_filename = this->get_lockname() + ".released";

    if (utils::file_exists(release_filename))
        throw std::runtime_error("Task " + this->taskname + " already released.");

    if (utils::file_exists(lock_filename)){
        std::remove(lock_filename.c_str());
    } else {
        throw std::runtime_error("Task " + this->taskname + " not locked.");
    }
    std::ofstream output(release_filename);
}

/** 
 * Remove the task from the registery
 */
void Nanny::clear_task(){
    std::string lock_filename = this->get_lockname() + ".lock";
    if (utils::file_exists(lock_filename))
        std::remove(lock_filename.c_str());
    std::string release_filename = this->get_lockname() + ".released";
    if (utils::file_exists(release_filename))
        std::remove(release_filename.c_str());
}


/**
 * check whether the task is locked
 */
bool Nanny::is_locked(){
    std::string lock_filename = this->get_lockname() + ".lock";
    return utils::file_exists(lock_filename);
}

/**
 * check whether the task is released
 */
bool Nanny::is_released(){
    std::string release_filename = this->get_lockname() + ".released";
    return utils::file_exists(release_filename);
}
