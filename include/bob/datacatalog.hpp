/**
 * Let's try to code some BOB emulator model in CPP
 */
#ifndef DataCatalog_HH
#define DataCatalog_HH
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor-blas/xlinalg.hpp>
#include <xtensor/xmanipulation.hpp>
#include <xtensor-io/xhighfive.hpp>
#include <highfive/H5Easy.hpp>
#include <string>
#include <iostream>
#include <map>
#include <set>
#include <math.h>

// Bob libraries
#include <pbar.hpp>
#include <bob/helpers.hpp>
#include <bob/star.hpp>
#include <bob/vaextable.hpp>

using namespace xt::placeholders;


typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;


/**
 * The interface of reading data from disk into a format compatible internally.
 *
 * This class is responsible for reading in the data and mapping the
 * various columns into understandable information for the code to run.
 */
class DataCatalog {

    public:
        std::string fname;
        VaexTable data;
        xt::xarray<std::string> mapping;

        DataCatalog(const std::string & fname, const xt::xarray<std::string> & mapping);
        DataCatalog(const VaexTable& table, const xt::xarray<std::string>& mapping);
        size_t size();
        Star get_star(size_t idx);
};

/**
 * Constructor
 */
DataCatalog::DataCatalog(const std::string & fname, const xt::xarray<std::string> & mapping):
    data(fname), fname(fname), mapping(mapping) {}


DataCatalog::DataCatalog(const VaexTable& table, const xt::xarray<std::string> & mapping):
    data(table), fname(table.filename), mapping(mapping) {}


size_t DataCatalog::size(){
    std::string which;
    std::string what;
    std::string what_err;

    for (size_t field_k=0; field_k < this->mapping.shape(0); ++field_k){
        which = this->mapping(field_k, 0);
        what = this->mapping(field_k, 1);
        what_err = this->mapping(field_k, 2);
        if (which != "name") {
            return this->data.get_data<IMatrix>(what).shape(0);
        }
    }
    return 0;
}

/**
 * Get a given star from entry idx
 */
Star DataCatalog::get_star(const size_t index){
    xt::xarray<std::string> sed_names = xt::view(this->mapping, xt::range(2, _), 0);
    size_t n_sed = sed_names.shape(0);

    std::string name;
    double parallax = 0.0;
    double parallax_error = 0.0;
    xt::xarray<double> sed_values = xt::empty<double>({n_sed});
    xt::xarray<double> sed_error = xt::empty<double>({n_sed});

    std::string which;
    std::string what;
    std::string what_err;

    std::vector<size_t> slice = {index, index + 1};

    for (size_t field_k=0; field_k < this->mapping.shape(0); ++field_k){
        which = this->mapping(field_k, 0);
        what = this->mapping(field_k, 1);
        what_err = this->mapping(field_k, 2);
        if (which == "name") {
            name = std::to_string(this->data.get_data<size_t>(what, slice));
        } else if (which == "parallax") {
            parallax = this->data.get_data<double>(what, slice);
            parallax_error = this->data.get_data<double>(what_err, slice);
        } else {
            sed_values[field_k - 2] = this->data.get_data<double>(what, slice);
            sed_error[field_k - 2] = this->data.get_data<double>(what_err, slice);
        }
    }


    return Star(name, parallax, parallax_error, sed_names, sed_values, sed_error);
}

/**
 * Nice representation of Catalog objects with cout
 */
std::ostream &operator<<(std::ostream &os, const DataCatalog &cat) {

    os << std::endl
       << "DataCatalog object " + cat.fname << std::endl
       << "--------------------------------" << std::endl
       << " Column Mapping: " <<std::endl;

    std::string which;
    std::string what;
    std::string what_err;
    size_t nchar1=0, nchar2=0, nchar3=0;
    for (size_t field_k=0; field_k < cat.mapping.shape(0); ++field_k){
        which = cat.mapping(field_k, 0);
        what = cat.mapping(field_k, 1);
        what_err = cat.mapping(field_k, 2);
        nchar1 = fmax(nchar1, which.size());
        nchar2 = fmax(nchar2, what.size());
        nchar3 = fmax(nchar3, what_err.size());
    }

    for (size_t field_k=0; field_k < cat.mapping.shape(0); ++field_k){
        which = cat.mapping(field_k, 0);
        what = cat.mapping(field_k, 1);
        what_err = cat.mapping(field_k, 2);
        os << "    " << std::setw(nchar1) << std::right << which
           << " | "  << std::setw(nchar2) << std::left << what
           << ", "   << std::setw(nchar3) << std::left << what_err << std::endl;
    }
    return os;
}

#endif // DataCatalog_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
