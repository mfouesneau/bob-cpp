/**
 * Reading Vaex hdf5 in a simple way
 */
#ifndef VaexTable_HH
#define VaexTable_HH
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor/xmanipulation.hpp>
#include <xtensor-io/xhighfive.hpp>
#include <highfive/H5Easy.hpp>
#include <string>
#include <iostream>
#include <unordered_map>
#include <set>


/**
 * Interface to Vaex hdf5 file format.
 */
class VaexTable {
    public:
        std::string filename;
        // std::vector<std::string> keys;
        std::set<std::string> keys;
        // data_types
        std::unordered_map<std::string, std::string> types;

        explicit VaexTable(const std::string &filename);
        VaexTable(const VaexTable & table);
        template<typename T> T get_data(const std::string & key);
        template<typename T> T get_data(const std::string & key,
                                        const std::vector<size_t> & idx);
        template<typename T> T get_data(const std::string & key,
                                        const size_t from, const size_t to);
        bool contains(const std::string & key) const;

    private:
        void get_data_types();
};

/**
 * Constructor
 */
VaexTable::VaexTable(const std::string &filename){
    this->filename = filename;
    HighFive::File file(filename, HighFive::File::ReadOnly);
    HighFive::Group data = file.getGroup("/table/columns");
    for(auto &name : data.listObjectNames()){this->keys.insert(name);}
    this->get_data_types();
}

/**
 * Constructor copy
 */
VaexTable::VaexTable(const VaexTable & table){
    this->filename = table.filename;
    HighFive::File file(filename, HighFive::File::ReadOnly);
    HighFive::Group data = file.getGroup("/table/columns");
    for(auto &name : data.listObjectNames()){this->keys.insert(name);}
    this->get_data_types();
}

/**
 * Read data types from the HDF5
 */
void VaexTable::get_data_types(){
    this->types.clear();
    HighFive::File file (this->filename, HighFive::File::ReadOnly);
    for (auto name: this->keys){
        HighFive::DataSet dataset = file.getDataSet("/table/columns/" + name + "/data");
        this->types[name] = dataset.getDataType().string();
    }
}

/**
 * Get data from name.
 *
 * This is a template function where the argument is the type of the data to
 * read.
 *
 * @param key   which field to read
 *
 * @return data from the file
 */
template<typename T>
T VaexTable::get_data(const std::string & key){
    if (!this->contains(key))
        throw std::runtime_error("Key " + key + " not found in the data");
    HighFive::File file(filename, HighFive::File::ReadOnly);
    T val = H5Easy::load<T>(file, "/table/columns/" + key + "/data");
    return val;
}

/**
 * Get data from name and slice
 *
 * This is a template function where the argument is the type of the data to
 * read.
 *
 * @param key   which field to read
 * @param idx   which slice to read (e.g., {0, 10})
 *
 * @return data from the file
 */
template<typename T>
T VaexTable::get_data(const std::string & key, const std::vector< size_t > &idx){
    if (!this->contains(key))
        throw std::runtime_error("Key " + key + " not found in the data");
    HighFive::File file(filename, HighFive::File::ReadOnly);
    T val = H5Easy::load<T>(file, "/table/columns/" + key + "/data", idx);
    return val;
}


/**
 * Get data from name and slice
 *
 * This is a template function where the argument is the type of the data to
 * read.
 *
 * @param key   which field to read
 * @param from  which slice start index
 * @param to    which slice end position
 *
 * @return data from the file
 */
template<typename T>
T VaexTable::get_data(const std::string & key, const size_t from, const size_t to){
    if (!this->contains(key))
        throw std::runtime_error("Key " + key + " not found in the data");
    HighFive::File file(filename, HighFive::File::ReadOnly);
    T val = H5Easy::load<T>(file, "/table/columns/" + key + "/data", {from, to});
    return val;
}

/**
 * Checks if a given key exists in the fields of the table
 */
bool VaexTable::contains(const std::string & key) const{
    auto container = this->keys;
    bool is_in = container.find(key) != container.end();
    return is_in;
}


/**
 * Nice representation of VaexTable objects with cout
 */
std::ostream &operator<<(std::ostream &os, const VaexTable &ds) {

    os << std::endl
       << "VaexTable object " + ds.filename << std::endl
       << "--------------------------------" << std::endl
       << "Columns" << std::endl;
    for(const auto& name: ds.keys){
	    os << "    " << name << std::endl;
    }
    return os << std::endl;
}

#endif  // VaexTable_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
