/**
 * Some probability distribution tools
 */
#ifndef DISTRIBUTIONS_HH
#define DISTRIBUTIONS_HH
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xmath.hpp>
#include <iostream>
#include <math.h>
#include <cmath>

typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;


/**
 * Necessary tools for normal distribution
 */
namespace normal {
    const double pi = 3.14159265358979323846;

    /**
     * Log probability distribution of a normal distribution.
     *
     * @param values   values to compute the log probabilities
     * @param mean     mean of the distribution
     * @param variance variance of the distribution
     *
     * @return log probabilities
     * */
    DMatrix logpdf(const DMatrix & values, const double mean=0., const double variance=1.){
        double ivar = 1. / variance;
        DMatrix m2lnp = xt::pow((values - mean), 2) * ivar - std::log(ivar) + std::log(2. * pi);
        return -0.5 * m2lnp;
    }


    /**
     * Log probability distribution of a normal distribution.
     *
     * @param values   values to compute the log probabilities
     * @param mean     mean of the distribution
     * @param variance variance of the distribution
     *
     * @return log probabilities
     * */
    double logpdf(const double values, const double mean=0., const double variance=1.){
        double ivar = 1. / variance;
        double m2lnp = std::pow((values - mean), 2) * ivar - std::log(ivar) + std::log(2. * pi);
        return -0.5 * m2lnp;
    }

    /**
     * Log probability distribution of a normal distribution.
     *
     * @param values   values to compute the log probabilities
     * @param mean     mean of the distribution
     * @param variance variance of the distribution
     *
     * @return log probabilities
     * */
    DMatrix logpdf(const DMatrix& values, const DMatrix& mean, const DMatrix& variance){
        DMatrix ivar = 1. / variance;
        DMatrix m2lnp = xt::pow((values - mean), 2) * ivar - xt::log(ivar) + std::log(2. * pi);
        return -0.5 * m2lnp;
    }

}
#endif // DISTRIBUTIONS_HH
// vim: expandtab:ts=4:softtabstop=4:shiftwidth=4
