/**
 * @file vaexcatalog.hpp
 * @brief  interfacee to csv catalog files
 * @version 0.1
 * @date 2020-11-18
 *
 * @copyright Copyright (c) 2020
 */
#pragma once
#include <bob/catalogs/base.hpp>
#include <bob/vaextable.hpp>


namespace Catalog {

    /**
     * The interface of reading data from disk into a format compatible internally.
     *
     * This class is responsible for reading in the data and mapping the
     * various columns into understandable information for the code to run.
     */
    class VaexCatalog : public Base {

        public:
            VaexTable data;

            VaexCatalog(const std::string & fname, const xt::xarray<std::string> & mapping);
            VaexCatalog(const VaexTable& table, const xt::xarray<std::string>& mapping);
            size_t size();
            Star get_star(size_t idx);
    };

    /**
     * Constructor
     */
    VaexCatalog::VaexCatalog(const std::string & fname, const xt::xarray<std::string> & mapping):
        Base(fname, mapping), data(fname){}

    VaexCatalog::VaexCatalog(const VaexTable& table, const xt::xarray<std::string> & mapping):
        Base(table.filename, mapping), data(table){}

    size_t VaexCatalog::size(){
        std::string which;
        std::string what;
        std::string what_err;

        for (size_t field_k=0; field_k < this->mapping.shape(0); ++field_k){
            which = this->mapping(field_k, 0);
            what = this->mapping(field_k, 1);
            what_err = this->mapping(field_k, 2);
            if (which != "name") {
                return this->data.get_data<IMatrix>(what).shape(0);
            }
        }
        return 0;
    }

    /**
     * Get a given star from entry idx
     */
    Star VaexCatalog::get_star(const size_t index){
        xt::xarray<std::string> sed_names = xt::view(this->mapping, xt::range(2, _), 0);
        size_t n_sed = sed_names.shape(0);

        std::string name;
        double parallax = 0.0;
        double parallax_error = 0.0;
        xt::xarray<double> sed_values = xt::empty<double>({n_sed});
        xt::xarray<double> sed_error = xt::empty<double>({n_sed});

        std::string which;
        std::string what;
        std::string what_err;

        std::vector<size_t> slice = {index, index + 1};

        for (size_t field_k=0; field_k < this->mapping.shape(0); ++field_k){
            which = this->mapping(field_k, 0);
            what = this->mapping(field_k, 1);
            what_err = this->mapping(field_k, 2);
            if (which == "name") {
                name = std::to_string(this->data.get_data<size_t>(what, slice));
            } else if (which == "parallax") {
                parallax = this->data.get_data<double>(what, slice);
                parallax_error = this->data.get_data<double>(what_err, slice);
            } else {
                sed_values[field_k - 2] = this->data.get_data<double>(what, slice);
                sed_error[field_k - 2] = this->data.get_data<double>(what_err, slice);
            }
        }
        return Star(name, parallax, parallax_error, sed_names, sed_values, sed_error);
    }
}; // end namespace Catalog