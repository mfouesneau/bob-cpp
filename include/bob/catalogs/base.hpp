#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <iterator>
#include <iostream>
#include <fstream>
#include <xtensor/xio.hpp>
#include <xtensor/xtensor.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xadapt.hpp>
#include <bob/star.hpp>
#include <mfio.hpp>

typedef double DType;
// linalg package doesn't support dynamic layouts
using DMatrix = xt::xarray<DType, xt::layout_type::row_major>;
using IMatrix = xt::xarray<int, xt::layout_type::row_major>;
using shape_type = std::vector<std::size_t>;
using namespace xt::placeholders;


namespace Catalog {

    /**
     * Base catalog class
     */
    class Base{
        public:
            std::string fname;
            xt::xarray<std::string> mapping;
            Base(){};
            Base(const std::string& fname, const xt::xarray<std::string>& mapping): fname(fname), mapping(mapping){};
            virtual size_t size() {return 0;}
            virtual Star get_star(size_t idx) {
                return Star("null", 0., 0., {}, DMatrix(), DMatrix());}
    };
}; // end namespace Catalog


/**
 * Nice representation of Catalog objects with cout
 */
std::ostream &operator<<(std::ostream &os, Catalog::Base& cat) {

    os << std::endl
       << "Catalog object " + cat.fname << std::endl
       << "--------------------------------" << std::endl
       << " Column Mapping: " <<std::endl;

    std::string which;
    std::string what;
    std::string what_err;
    size_t nchar1=0, nchar2=0, nchar3=0;
    for (size_t field_k=0; field_k < cat.mapping.shape(0); ++field_k){
        which = cat.mapping(field_k, 0);
        what = cat.mapping(field_k, 1);
        what_err = cat.mapping(field_k, 2);
        nchar1 = fmax(nchar1, which.size());
        nchar2 = fmax(nchar2, what.size());
        nchar3 = fmax(nchar3, what_err.size());
    }

    for (size_t field_k=0; field_k < cat.mapping.shape(0); ++field_k){
        which = cat.mapping(field_k, 0);
        what = cat.mapping(field_k, 1);
        what_err = cat.mapping(field_k, 2);
        os << "    " << std::setw(nchar1) << std::right << which
           << " | "  << std::setw(nchar2) << std::left << what
           << ", "   << std::setw(nchar3) << std::left << what_err << std::endl;
    }
    os.imbue(std::locale(""));
    os << "\nNumber of catalog entries: " << cat.size() << std::endl;
    os.imbue(std::locale());
    return os;
}