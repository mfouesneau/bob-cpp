/**
 * @file csvcatalog.hpp
 * @author your name (you@domain.com)
 * @brief  interfacee to csv catalog files
 * @version 0.1
 * @date 2020-11-18
 *
 * @copyright Copyright (c) 2020
 */
#pragma once
#include <bob/catalogs/base.hpp>
#include <mfio.hpp>


namespace Catalog {

    /**
     * Catalog interface to CSV input data
     */
    class CsvCatalog : public Base {

        public:
            CsvCatalog(const std::string& fname, const xt::xarray<std::string>& mapping);
            size_t size() {return this->catalog_size;};
            Star get_star(size_t idx);

        private:
            io::AsciiReader data;
            size_t catalog_size = 0;
            size_t current_idx = 0;
            std::map<std::string, std::string> row;

            void seek_row(size_t index);

    };

    /**
     * @brief Construct a new Csv Catalog:: Csv Catalog object
     *
     * @param fname      which ascii file to read
     * @param mapping    mapping of the catalog
     */
    CsvCatalog::CsvCatalog(const std::string& fname,
                           const xt::xarray<std::string>& mapping):
                           Base(fname, mapping), data(fname) {
        this->catalog_size = this->data.count_lines() - 1;  // header line removed
        this->seek_row(0);
    };


    /**
     * Move the pointer from the ascii file and load the row data
     * Keeps a cache of the current data (avoids re-reading) and
     * jumps and resets the open file if needed.
     *
     * @param idx which data row to load
     */
    void CsvCatalog::seek_row(size_t idx){

        // Reset stream if needed
        if (idx < this->current_idx){
            this->data = io::AsciiReader(this->fname);
            this->data.set_comment("#");
            this->data.set_delimiter(",");
            this->catalog_size = this->data.count_lines() - 1;  // header line removed
            ++this->data; // remove header line
            this->row.clear();
            this->current_idx = 0;
        }

        // move index
        while(this->current_idx <= idx){
            this->current_idx++;
            ++this->data;
            this->row.clear();
        }

        // Check if data in cache already.
        if (this->row.empty()){
            auto cat_columns = this->data.get_header();
            // read line into a map(name, value)
            std::string value;
            for (size_t k=0; k<cat_columns.size(); k++){
                this->data.get_next_value<std::string>(value);
                this->row[cat_columns[k]] = value;
            }
        }

    }

    /**
     * Generates the Star object from the catalog
     *
     * @param idx    which entry to load
     * @return Star  the star
     */
    Star CsvCatalog::get_star(size_t idx){

        this->seek_row(idx);

        // convert to Star
        xt::xarray<std::string> sed_names = xt::view(mapping, xt::range(2, _), 0);
        size_t n_sed = sed_names.shape(0);

        std::string name;
        double parallax = 0.0;
        double parallax_error = 0.0;
        xt::xarray<double> sed_values = xt::empty<double>({n_sed});
        xt::xarray<double> sed_error = xt::empty<double>({n_sed});

        std::string which;
        std::string what;
        std::string what_err;

        for (size_t field_k=0; field_k < mapping.shape(0); ++field_k){
            which = mapping(field_k, 0);
            what = mapping(field_k, 1);
            what_err = mapping(field_k, 2);
            if (which == "name") {
                name = this->row.at(what);
            } else if (which.compare("parallax") == 0) {
                parallax = io::parse<double>(this->row.at(what));
                parallax_error = io::parse<double>(this->row.at(what_err));
            } else {
                sed_values[field_k - 2] = io::parse<double>(this->row.at(what));
                sed_error[field_k - 2] = io::parse<double>(this->row.at(what_err));
            }
        }

        return Star(name, parallax, parallax_error, sed_names, sed_values, sed_error);

    }

}; // end namespace Catalog